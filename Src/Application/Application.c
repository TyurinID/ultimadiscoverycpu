/**
  ************************************************************************************************
  * @file	 	 Application.c
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    24-January-2017
  * @brief   General application thread function.
  * 		 		 Periodicaly gets data from MainCPU.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include "Application/Application.h" 
#include <math.h>
#include "Uart/Modbus.h"  
#include "GraphicTask/Graph.h"

#include "GraphicTask/AmpGraphPage.h"
//#include "GraphicTask/FlowMeterPage.h"
//#include "GraphicTask/DelayGraphPage.h"
#include "GraphicTask/ProfilePage.h" 
#include "GraphicTask/MeasurePage.h"

	
// --------------------------------------------------------- Global variables --------------------:
bool gGetNetworkSett;
bool gSetNetworkSett;
  
// --------------------------------------------------------- Private variables -------------------:  
static uint32_t Timer;
//static uint8_t	InjectorNum;		
static uint16_t graph_value[GRAPH_POINTS_NUM_MAX];


	
// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:

 /***********************
  *		@brief  Default Inititalization.
  *  	@param  None
  *  	@retval None
  **********************/
void	ApplicationInit(void)
{
	gGetNetworkSett = true;
	gSetNetworkSett = false;
}

  
 /***********************
  *		@brief  Periodicaly gets data from MainCPU.
  *  	@param  argument
  *  	@retval None
  **********************/
void	ApplicationThread(void const *argument)
{
	osDelay(APPL_START_DELAY_MS);
	ApplicationInit();
	
	while(1)
	{		
		// Reading and setting network information from/to MainCPU:
		if(gSetNetworkSett)
		{
			gSetNetworkSett = false;
			gGetNetworkSett = true;
			uint16_t temp_coil = 0xFF00;			
			uint16_t temp_param[2];
			temp_param[0] = ((uint16_t)GIpAddress.byte2)<<8U | (uint16_t)GIpAddress.byte1;
			temp_param[1] = ((uint16_t)GIpAddress.byte4)<<8U | (uint16_t)GIpAddress.byte3;			
			doModbus(WRITE_MULTIPLE_REGISTERS, 160, 2, temp_param);
			temp_param[0] = ((uint16_t)GNetMask.byte2)<<8U | (uint16_t)GNetMask.byte1;
			temp_param[1] = ((uint16_t)GNetMask.byte4)<<8U | (uint16_t)GNetMask.byte3;	
			doModbus(WRITE_MULTIPLE_REGISTERS, 162, 2, temp_param);
			temp_param[0] = ((uint16_t)GDefaultGateway.byte2)<<8U | (uint16_t)GDefaultGateway.byte1;
			temp_param[1] = ((uint16_t)GDefaultGateway.byte4)<<8U | (uint16_t)GDefaultGateway.byte3;	
			doModbus(WRITE_MULTIPLE_REGISTERS, 164, 2, temp_param);
			doModbus(WRITE_SINGLE_COIL, 100, 1, &temp_coil);
		}
		if(gGetNetworkSett)
		{
			gGetNetworkSett = false;					
			uint16_t temp_param[2];
			doModbus(READ_HOLDING_REGISTERS, 160, 2, temp_param);
			GIpAddress.byte1 = (uint8_t)temp_param[0];			
			GIpAddress.byte2 = temp_param[0]>>8U;
			GIpAddress.byte3 = (uint8_t)temp_param[1];			
			GIpAddress.byte4 = temp_param[1]>>8U;;
			doModbus(READ_HOLDING_REGISTERS, 162, 2, temp_param);
			GNetMask.byte1 = (uint8_t)temp_param[0];			
			GNetMask.byte2 = temp_param[0]>>8U;;
			GNetMask.byte3 = (uint8_t)temp_param[1];			
			GNetMask.byte4 = temp_param[1]>>8U;;
			doModbus(READ_HOLDING_REGISTERS, 164, 2, temp_param);
			GDefaultGateway.byte1 = (uint8_t)temp_param[0];			
			GDefaultGateway.byte2 = temp_param[0]>>8U;
			GDefaultGateway.byte3 = (uint8_t)temp_param[1];			
			GDefaultGateway.byte4 = temp_param[1]>>8U;
		}
		
		
		// Periodicaly read information from MainCPU =============================================:
		if(Timer % (APPL_AMP_GRAPH_PERIOD_MS/APPL_PERIOD_MS) == 0)
		{		
			
			// Reading amp graph information from MainCPU ===================================:
			// for many injectors: read 8/28 register and choose curve on Graph;
			// Is amp graph ready:
			uint16_t graph_ready = 1;
			doModbus(READ_DISCRETE_INPUTS, 2, 1, &graph_ready);
			if(graph_ready != 0)	// amp graph ready
			{
				// Read graph endtime:
				uint16_t graph_endtime = APPL_AMP_GRAPH_X_US;		// in ms	// static = 2000us
				// Read graph values:
				uint16_t graph_points_num = floorf(((float)graph_endtime/0.95f)/APPL_AMP_GRAPH_SKIP_POINTS);
				uint16_t pack1 = graph_points_num/4 + graph_points_num%4;
				doModbus(READ_INPUT_REGISTERS, 6349, graph_points_num/4, graph_value);
				doModbus(READ_INPUT_REGISTERS, 6349+graph_points_num/4, graph_points_num/4, graph_value+graph_points_num/4);
				doModbus(READ_INPUT_REGISTERS, 6349+graph_points_num/2, graph_points_num/4, graph_value+graph_points_num/2);
				doModbus(READ_INPUT_REGISTERS, 6349+(graph_points_num/4)*3, pack1, graph_value+(graph_points_num/4)*3);				
				// Read injector type:
				tInjType inj_type;
				doModbus(READ_HOLDING_REGISTERS, 43, 1, (uint16_t*)&inj_type);
				uint16_t graph_measured_points;
				doModbus(READ_HOLDING_REGISTERS, 5, 1, &graph_measured_points);
				graph_measured_points = floorf(((float)graph_measured_points/0.95f)/APPL_AMP_GRAPH_SKIP_POINTS);
				// Graph reinitialization ==========:
				vTaskSuspendAll();
				gAmpGraph.PointsNum = graph_points_num;
				for(uint16_t i=0; i<graph_points_num; i++)
				{
					gAmpGraph.xValue[i] = i*0.95f*APPL_AMP_GRAPH_SKIP_POINTS;
					gAmpGraph.yValue[i] = (float)graph_value[i]/93.07f;
				}
				if(inj_type == INJ_PIEZO)
				{			//	Draw second part of Amp graph by horizontal reflection:
					for(uint16_t i=graph_measured_points; i<graph_points_num; i++)
						gAmpGraph.yValue[i] = -gAmpGraph.yValue[i - graph_measured_points];
				}
				InitializeGraph(&gAmpGraph);
				xTaskResumeAll();
			}
			
		
			// Reading Profile information from MainCPU ===================================:
			uint16_t temp_data[12];
			doModbus(READ_HOLDING_REGISTERS, 1, 4, temp_data);			
			doModbus(READ_HOLDING_REGISTERS, 120, 8, temp_data+4);
			uint16_t temp;
			for(uint8_t i=0; i<4; i++)
			{
				temp = temp_data[4+2*i];
				temp_data[4+2*i] = temp_data[5+2*i];
				temp_data[5+2*i] = temp;				
			};
			vTaskSuspendAll();
			gProfile.BoostI = roundf((float)temp_data[0]/9.307f)/10;
			gProfile.FirstI = roundf((float)temp_data[1]/9.307f)/10;
			gProfile.SecondI = roundf((float)temp_data[2]/9.307f)/10;
			gProfile.FirstW = roundf((float)temp_data[3]);
			gProfile.BoostU = roundf(*((float*)(temp_data+4)));
			gProfile.BatteryU = roundf(*((float*)(temp_data+6)));
			gProfile.NegativeU1 = roundf(*((float*)(temp_data+8)));
			gProfile.NegativeU2 = roundf(*((float*)(temp_data+10)));
			xTaskResumeAll();
			
			// Reading Measured information from MainCPU ===================================:
			tInjType inj_type = INJ_COIL;
			doModbus(READ_HOLDING_REGISTERS, 43, 1, (uint16_t*)&inj_type);
			uint16_t inj_measured_num;
			doModbus(READ_HOLDING_REGISTERS, 41, 1, &inj_measured_num);			
			uint16_t temp_param[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
			if(inj_type == INJ_COIL)
			{
				doModbus(READ_INPUT_REGISTERS, 6300, 2, (temp_param)+(inj_measured_num-1)*2);
				doModbus(READ_INPUT_REGISTERS, 6302, 2, (temp_param)+(inj_measured_num-1)*2+8);
			}
			else
			{
				doModbus(READ_INPUT_REGISTERS, 6304, 2, (temp_param)+(inj_measured_num-1)*2);
				doModbus(READ_INPUT_REGISTERS, 6306, 2, (temp_param)+(inj_measured_num-1)*2+8);
			}		
			for(uint8_t i=0; i<8; i++)
			{
				temp = temp_param[2*i];
				temp_param[2*i] = temp_param[1+2*i];
				temp_param[1+2*i] = temp;				
			};
			vTaskSuspendAll();
			gMeasured.InjType = inj_type;
			for(uint8_t i=0; i<INJECTORS_NUM; i++)
			{
				gMeasured.Param1[i] = roundf(*((float*)(temp_param+i*2)) *10)/10;
				if(inj_type == INJ_COIL)
					gMeasured.Param2[i] = roundf(*((float*)(temp_param+i*2+8)) *100)/100;
				else
					gMeasured.Param2[i] = roundf(*((float*)(temp_param+i*2+8)) *10)/10;
			}
			xTaskResumeAll();			
			
		}	
		osDelay(APPL_PERIOD_MS);
		Timer++;
	}
}

// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
