/**
  ************************************************************************************************
  * @file	 	 Application.h
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    24-January-2017
  * @brief   Head file for Application.c.
  *************************************************************************************************
  */
#ifndef APPLICATION_H
#define APPLICATION_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "cmsis_os.h"


// -------------------------------------------------------- Exported define ----------------------:
// Time ===============:
#define APPL_START_DELAY_MS				2500
#define APPL_PERIOD_MS						2			//in ms

// Graph ==============:
#define APPL_AMP_GRAPH_PERIOD_MS	5000
#define APPL_AMP_GRAPH_SKIP_POINTS	10
#define APPL_AMP_GRAPH_MAX_NUM		( (uint16_t)(2048/APPL_AMP_GRAPH_SKIP_POINTS) )
#define APPL_AMP_GRAPH_X_US				1930		// state top value of abscissa axis

	
// -------------------------------------------------------- Exported types -----------------------: 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern bool gGetNetworkSett;
extern bool gSetNetworkSett;



// -------------------------------------------------------- Exported functions -------------------: 
extern void	ApplicationThread(void const *argument);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
