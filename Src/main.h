/**
  ************************************************************************************************
  * @file	 	 main.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    20-December-2016
  * @brief   Header for main.c.
  * 				 Define: threads delays;
	*									 threads periods;
	*									 number of using channels;
  *************************************************************************************************
  */
#ifndef __MAIN_H
#define __MAIN_H

// -------------------------------------------------------- Includes -----------------------------: 
#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_sdram.h"
#include "stm32746g_discovery_lcd.h"
#include <stdbool.h>
#include "GUI.h"


// -------------------------------------------------------- Exported define ----------------------:
// Comment, if ethernet not used:
// #define ETHERNET_TCP_IP


// -------------------------------------------------------- Exported variables -------------------: 

#endif /* __MAIN_H */


/**********************************************************************************END OF FILE****/ 
