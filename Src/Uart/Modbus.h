/**
  ************************************************************************************************
  * @file	 	 Modbus.h
  * @author  Tyurin Ivan
  * @version V1.2
  * @date    15-December-2016
  * @brief   Header for Modbus.c.
	*					 Configure number of registers here.
	*					 Configure device modbus address.
  *************************************************************************************************
  */
#ifndef __Modbus
#define __Modbus

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"
#include "Uart/uart.h"

// -------------------------------------------------------- Exported define ----------------------:
#define MODBUS_DEVICE_ADDRESS					101

#define MODBUS_MAIN_CPU_ADDRES				100

#define MODBUS_DISCRETE_IPUTS_NUM				0
#define MODBUS_COILS_NUM 								0
#define MODBUS_INPUT_REGISTERS_NUM				0			// TI!
#define MODBUS_HOLDING_REGISTERS_NUM 			0

#define EXEPTION_CODE_01		0x01
#define EXEPTION_CODE_02		0x02
#define EXEPTION_CODE_03		0x03
#define EXEPTION_CODE_04		0x04
#define EXEPTION_CODE_05		0x05

#define MODBUS_ERROR_CODE		0x80

// -------------------------------------------------------- Exported types -----------------------:
// Stage of function execution ==:
typedef enum
{
	MODBUS_FORM_REQUEST,
	MODBUS_GET_ANSWER
} tModbusAskStage;	

// Structure of Input and Holding Modbus register's Tags =============:
typedef struct
{
	uint16_t *Value;  	// Pointer to item's Value.
}
tUint16Tag;

// Structure of Modbus Coils and Discrete Inputs Tags ================:
typedef struct
{
	_Bool *Value;  	// Pointer to item's Value.
}
tBoolTag;

typedef enum
{
	READ_COILS	= 0x01,
	READ_DISCRETE_INPUTS,
	READ_HOLDING_REGISTERS,
	READ_INPUT_REGISTERS,
	WRITE_SINGLE_COIL,
	WRITE_SINGLE_REGISTER,
	READ_EXCEPTION_STATUS,	
	MODBUS_DIAGNOSTIC,
	GET_COM_EVENT_COUNTER = 0x0b,
	GET_COM_EVENT_LOG = 0x0c,
	WRITE_MULTIPLE_REGISTERS = 0x10,
	MODBUS_NONE
}
tModbusFuncCode;
// -------------------------------------------------------- Private macros -----------------------:



// -------------------------------------------------------- Exported variables -------------------:
extern bool gModbusError;


// -------------------------------------------------------- Exported functions -------------------:
extern void Modbus_Init(void);
int ModbusAsk(tModbusFuncCode func,	uint8_t	device_addr, uint16_t reg_start, uint16_t reg_num, uint16_t* data, tUart* uart, uint16_t cycles_timeout);
extern void CreateModbusInputRegTag16(uint16_t *source, uint16_t index);
extern uint16_t ModbusCRC(uint8_t *buf, uint16_t len);		 // Compute the MODBUS RTU CRC.

extern void doModbus(tModbusFuncCode func, uint16_t reg_start, uint8_t reg_num, uint16_t* data);


// -------------------------------------------------------- Exported constants -------------------:


#endif /* __Modbus */

/**********************************************************************************END OF FILE****/
