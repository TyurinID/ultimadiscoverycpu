/**
  ************************************************************************************************
  * @file	 	 uart.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    19-January-2017
  * @brief   Functions for DUPLEX uart conversation.
  * 		 			UART interrupt functions.
  *************************************************************************************************
  */
	
  
// -------------------------------------------------------- Includes -----------------------------:  
#include "Uart/uart.h"
#include <string.h>
#include <stdlib.h>
  
// --------------------------------------------------------- Global variables --------------------:
tUart gUart;			// Main structure for Uart communication.
tUART_Error 	gUART_Error;

// --------------------------------------------------------- Private variables -------------------:  
static 	UART_HandleTypeDef UartHandle;
static 	uint32_t start_time;
static	uint16_t rxDMAPosition;

  
// --------------------------------------------------------- Private function prototypes ---------:	
//static tUART_Error parseMessage(tUart* uart);		
//static void formMessage(tUart* uart);
//static uint8_t crc8d5(uint8_t *pcBlock, unsigned int len, uint8_t *crc);
  
// --------------------------------------------------------- Global functions --------------------:


 /***********************
  *		@brief  Initialize aurt structure.
  *  	@param  None
  *  	@retval None
  **********************/
uint8_t UartInit(void)
{
  UartHandle.Instance        = USARTx;
  UartHandle.Init.BaudRate   = 9600;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;
  UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
  {
    return 0;
  }  
  if(HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    return 0;
  }	
	
	// Initialization default values ================:	
	for(uint16_t i=0; i<UART_RX_MAX_SIZE; i++)
	{
		gUart.RxBuff[i] = 0xff;
		gUart.RxMessage[i] = 0xff;
	}
	for(uint16_t i=0; i<UART_TX_MAX_SIZE; i++)
		gUart.TxBuff[i] = 0xff;
	
//	gUart.TxBuff[0] = 'M';
//	gUart.TxBuff[1] = 'A';
//	gUart.TxBuff[2] = 'I';
//	gUart.TxBuff[3] = 'N';
//	gUart.Command = UART_GET_IP;
	gUart.Ready = SET;
	gUart.Stage = RECEIVE;
	gUART_Error = UART_OK;
	rxDMAPosition = 0;
	
	return 1;
}


 /***********************
  *		@brief  Do uart conversation with MainCPU.
  *  	@param  argument
  *  	@retval None
  **********************/
void UART_Thread(void const *argument)
{
	UartInit();
	osDelay(UART_START_DELAY_MS);
	
	// Start reception ======================================================================:
	if(HAL_UART_Receive_DMA(&UartHandle, gUart.RxBuff, UART_RX_MAX_SIZE) != HAL_OK)
	{
		gUART_Error = UART_RX_ERR;
		gUart.Stage = NONE;			// TI!
	}
	UartHandle.Instance->ICR |=(UART_FLAG_IDLE); 
	
	while(1)
	{
		switch(gUart.Stage)				// UART works stage by stage. 
		{			
			// Wait for Rx message ======================================================================:
			case RECEIVE:
				if(__HAL_UART_GET_FLAG(&UartHandle, UART_FLAG_IDLE))		// If message received:
				{
					UartHandle.Instance->ICR |=(UART_FLAG_IDLE); 
					
					// Get size of message:
					gUart.RxMsgSize = UART_RX_MAX_SIZE - UartHandle.hdmarx->Instance->NDTR - rxDMAPosition;
					if(gUart.RxMsgSize < 0)
						gUart.RxMsgSize +=UART_RX_MAX_SIZE;
					if(gUart.RxMsgSize <= 1)
						break;					
					uint16_t cnt = rxDMAPosition;
					for(uint16_t i=0; i<gUart.RxMsgSize; i++)
					{
						gUart.RxMessage[i] = gUart.RxBuff[cnt++];
						gUart.RxBuff[cnt-1] = 0xff; 
						if(cnt >= UART_RX_MAX_SIZE)
							cnt=0;
					}
					// Calculate new first bit position:
					rxDMAPosition = UART_RX_MAX_SIZE - UartHandle.hdmarx->Instance->NDTR;
					
					// Set the flag =:
					gUart.MsgReceived = true;
					
//					// Parse message and form answer =====================================:					
//					if ((gUART_Error = parseMessage(&gUart)) != UART_OK)		
//						gUart.Stage = RECEIVE;		// if any error.
				}
				else	// Here uart task spends  most of the time ===================!!
					osDelay(2);	
				break;
								
			// Sending message ========================================================================:
			case SEND:			
				gUart.Ready = RESET;
//				formMessage(&gUart);
				start_time = xTaskGetTickCount();
				if(HAL_UART_Transmit_DMA(&UartHandle, gUart.TxBuff, gUart.TxMsgSize) != HAL_OK)			// Start the transmission process.
				{
					gUART_Error = UART_TX_ERR;		
					gUart.Stage = RECEIVE;
				}
				else
					gUart.Stage = WAIT_TX;
				break;
			
			// Wait for end of message transfer ========================================================:
			case WAIT_TX:
				if(gUart.Ready != SET)		
				{						
					if( (xTaskGetTickCount() - start_time) > UART_TX_TIMEOUT_MS)
					{	
						gUART_Error = TIMEOUT_ERR;
						gUart.Stage = RECEIVE;
					}
				}
				else
				{
					for(uint16_t i=0; i<gUart.TxMsgSize; i++)
						gUart.TxBuff[i] = 0xff;
					gUart.Stage = RECEIVE;
				}
				break;
	
			// Error receive initialization ============================================================:
			case NONE:
			default:
				osDelay(2);
		}
//		osDelay(UART_PERIOD_MS);							
	}
}		


// --------------------------------------------------------- Private functions -------------------:

// /***********************
//  *		@brief  Parse message and set command flag.
//	*						If answer is needed, set uart.Stage = SEND.
//  *  	@param  uart - structure.
//  *  	@retval tUART_Error
//  **********************/
//static tUART_Error parseMessage(tUart* uart)
//{	
//	tUartCommand rx_command;
//	// Check msg crc:	
//	uint8_t crc = 0;		
//	crc = crc8d5(uart->RxMessage, uart->RxMsgSize - 1, &crc);
//	if( crc != uart->RxMessage[uart->RxMsgSize - 1] )
//		return UART_CRC_ERR;
//	// Check for "MAIN":
//	if(uart->RxMessage[0] != 'D' || uart->RxMessage[1] != 'I' || uart->RxMessage[2] != 'S' || uart->RxMessage[3] != 'C')
//		return UART_PARSING_ERR;
//	// Get command:
//	rx_command = (tUartCommand)uart->RxMessage[4];
//	
//	// Do action:
//	switch(rx_command)
//	{
//		case UART_GET_IP:
//			if(uart->Command != UART_GET_IP)		// if we didn't ask for ip
//				return	UART_WRONG_ANSWER_ERR;
//			GIpAddress.byte1 = (uint32_t)uart->RxMessage[5];
//			GIpAddress.byte2 = (uint32_t)uart->RxMessage[6];
//			GIpAddress.byte3 = (uint32_t)uart->RxMessage[7];
//			GIpAddress.byte4 = (uint32_t)uart->RxMessage[8];
//	
//			GNetMask.byte1 = (uint32_t)uart->RxMessage[9];
//			GNetMask.byte2 = (uint32_t)uart->RxMessage[10];
//			GNetMask.byte3 = (uint32_t)uart->RxMessage[11];
//			GNetMask.byte4 = (uint32_t)uart->RxMessage[12];
//	
//			GDefaultGateway.byte1 = (uint32_t)uart->RxMessage[13];
//			GDefaultGateway.byte2 = (uint32_t)uart->RxMessage[14];
//			GDefaultGateway.byte3 = (uint32_t)uart->RxMessage[15];
//			GDefaultGateway.byte4 = (uint32_t)uart->RxMessage[16];
//			break;
//		
//		case UART_RESET_IP:
//			if(uart->Command != UART_RESET_IP)		// if we didn't send new ip
//				return	UART_WRONG_ANSWER_ERR;
//			break;
//		
//		case UART_UNCKNOWN_COMMAND:
//			return	UART_UNCKNOWN_COMMAND_ERR;
//		
//		default:
//			return	UART_UNCKNOWN_COMMAND_ERR;
//	}
//	return UART_OK;
//}

// /***********************
//  *		@brief  Form sending msg.
//  *  	@param  uart - structure.
//  *  	@retval tUART_Error
//  **********************/
//static void formMessage(tUart* uart)
//{
//	uart->TxBuff[4] = uart->Command;

//	switch(uart->Command)
//	{	
//		case UART_RESET_IP:
//			uart->TxMsgSize = UART_RESET_IP_MSG_SIZE;
//			uart->TxBuff[5] = (uint8_t)(GIpAddress.byte1);
//			uart->TxBuff[6] = (uint8_t)(GIpAddress.byte2); 
//			uart->TxBuff[7] = (uint8_t)(GIpAddress.byte3);
//			uart->TxBuff[8] = (uint8_t)(GIpAddress.byte4);
//			uart->TxBuff[9] = (uint8_t)(GNetMask.byte1);
//			uart->TxBuff[10] = (uint8_t)(GNetMask.byte2);
//			uart->TxBuff[11] = (uint8_t)(GNetMask.byte3);
//			uart->TxBuff[12] = (uint8_t)(GNetMask.byte4);
//			uart->TxBuff[13] = (uint8_t)(GDefaultGateway.byte1);
//			uart->TxBuff[14] = (uint8_t)(GDefaultGateway.byte2);
//			uart->TxBuff[15] = (uint8_t)(GDefaultGateway.byte3);
//			uart->TxBuff[16] = (uint8_t)(GDefaultGateway.byte4);
//			uart->TxBuff[17] = 0;
//			uart->TxBuff[17] = crc8d5(uart->TxBuff, 17, &(uart->TxBuff[17]));
//			break;
//		
//		case UART_GET_IP:
//			uart->TxMsgSize = UART_GET_IP_MSG_SIZE;
//			uart->TxBuff[5] = 0;
//			uart->TxBuff[5] = crc8d5(uart->TxBuff, 5, &(uart->TxBuff[5]));
//			break;
//		
//		default:
//			uart->TxMsgSize = 6;
//			uart->TxBuff[5] = 0;
//			uart->TxBuff[5] = crc8d5(uart->TxBuff, 5, &(uart->TxBuff[5]));
//			break;
//	}
//}

// /***********************
//  *		@brief  Calculate crc code.
//  *  	@param  pcBlock - pointer to array;
//  *						len - length of array;
//	*						crc - pointer to variable for crc saving.
//  *  	@retval crc code.
//  **********************/
//static uint8_t crc8d5(uint8_t *pcBlock, unsigned int len, uint8_t *crc)
//{
//	unsigned int i;
// 
//  while (len--)
//  {
//    *crc ^= *pcBlock++;
// 
//    for (i = 0; i < 8; i++)
//        *crc = *crc & 0x80 ? (*crc << 1) ^ 0xd5 : *crc << 1;
//  }
//  return *crc;
//}


/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of DMA Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: trasfer complete*/
  gUart.Ready = SET;  
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: trasfer complete*/
  gUart.Ready = SET;  
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
//void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
//{
    //Error_Handler();
//}




/**
  * @brief  This function handles DMA interrupt request.  
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data transmission     
  */
void USARTx_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(UartHandle.hdmarx);
}

/**
  * @brief  This function handles DMA interrupt request.
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data reception    
  */
void USARTx_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(UartHandle.hdmatx);
}


/**
  * @brief  This function handles UART interrupt request.  
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data transmission     
  */
void USARTx_IRQHandler(void)
{
  HAL_UART_IRQHandler(&UartHandle);
}






