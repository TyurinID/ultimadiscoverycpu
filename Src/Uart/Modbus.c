/**
  *************************************************************************************************
  * @file	 	 Modbus.c
  * @author  Tyurin Ivan
  * @version V1.2
  * @date    15-December-2016
  * @brief   Library for Modbus protocol. Generate Modbus registers.
  * 				 Modbus request and response functions definitions.
  *************************************************************************************************
  */
				// Modbus + UART 115200 = 100 hold registers/ 30 ms.
				//												1 hold register/ 12 ms

// -------------------------------------------------------- Includes -----------------------------:
#include "Uart/Modbus.h"


// --------------------------------------------------------- Global variables --------------------:
bool gModbusError;
// Modbus registers declaration ============================================:
	//static tBoolTag	ModbusDiscreteInut[MODBUS_DISCRETE_IPUTS_NUM];
	//static tBoolTag	ModbusCoil[MODBUS_COILS_NUM];
	//static tUint16Tag	ModbusInutRegister[MODBUS_INPUT_REGISTERS_NUM];
	//static tUint16Tag	ModbusHoldingRegister[MODBUS_HOLDING_REGISTERS_NUM];


// --------------------------------------------------------- Private variables -------------------:
tUartCommand	Command;
//static uint16_t dummyValue = 0xffff;

// --------------------------------------------------------- Private function prototypes ---------:
//static uint16_t getValue16(tUint16Tag modbus_register);


// --------------------------------------------------------- Global functions --------------------:
/**********************************
  *	&	@brief  Initialize modbus registers by dummy values.
  * & 	@param  None
  * & 	@retval None
  *********************************/
void Modbus_Init()
{
//	for(uint16_t i=0; i<MODBUS_INPUT_REGISTERS_NUM; i++)
//		ModbusInutRegister[i].Value = &dummyValue;
}


/**********************************
  *	&		@brief  General Modbus Ask Function.
  *							Execute it cyclically.
  * & 	@param  None
  * & 	@retval Returns 0 if ask is in progress;
	*							returns 1 if ask is finished and data is transfered;
	*							returns -1 if any errors;
	*							returns -2 in case of timeout;
	*							returns -3 in case of incorrect registers number.
  *********************************/
int ModbusAsk(tModbusFuncCode func,	uint8_t	device_addr, uint16_t reg_start, uint16_t reg_num, uint16_t* data, tUart* uart, uint16_t cycles_timeout)
{	
	static tModbusAskStage stage = MODBUS_FORM_REQUEST;
	static uint16_t cycles_timer = 0;
	int res = 0;
	// Check for timeout:
	if(cycles_timer++ == cycles_timeout)
	{
		stage = MODBUS_FORM_REQUEST;
		cycles_timer = 0;
		return -2;
	}
	// Close function, if nothing to do:
	if(func == MODBUS_NONE)
	{
		stage = MODBUS_FORM_REQUEST;
		cycles_timer = 0;
		return 1;
	}

		
	switch(stage)
	{
		// Form the request ==============================================:
		case MODBUS_FORM_REQUEST:		
			uart->TxBuff[0] = device_addr;		
			uart->TxBuff[1] = func;
			switch(func)
			{
				// ================================ READ_COILS:
				case READ_COILS:
					// Check the number of registers to be read:
					if(reg_num>2000)
						return -3;
					uart->TxMsgSize = 8;
					uart->TxBuff[2] = (uint8_t)(reg_start>>8U);
					uart->TxBuff[3] = (uint8_t)(reg_start);
					uart->TxBuff[4] = (uint8_t)(reg_num>>8U);
					uart->TxBuff[5] = (uint8_t)(reg_num);	
					break;				
				
				// ================================ READ_DISCRETE_INPUTS:
				case READ_DISCRETE_INPUTS:
					// Check the number of registers to be read:
					if(reg_num>2000)
						return -3;
					uart->TxMsgSize = 8;
					uart->TxBuff[2] = (uint8_t)(reg_start>>8U);
					uart->TxBuff[3] = (uint8_t)(reg_start);
					uart->TxBuff[4] = (uint8_t)(reg_num>>8U);
					uart->TxBuff[5] = (uint8_t)(reg_num);	
					break;	
				
				// ================================ READ_HOLDING_REGISTERS:
				case READ_HOLDING_REGISTERS:
					// Check the number of registers to be read:
					if(reg_num>125)
						return -3;
					uart->TxMsgSize = 8;
					uart->TxBuff[2] = (uint8_t)(reg_start>>8U);
					uart->TxBuff[3] = (uint8_t)(reg_start);
					uart->TxBuff[4] = (uint8_t)(reg_num>>8U);
					uart->TxBuff[5] = (uint8_t)(reg_num);	
				// todo: reset error
					break;				
				
				// ================================ READ_INPUT_REGISTERS:
				case READ_INPUT_REGISTERS:
					// Check the number of registers to be read:
					if(reg_num>125)
						return -3;
					uart->TxMsgSize = 8;
					uart->TxBuff[2] = (uint8_t)(reg_start>>8U);
					uart->TxBuff[3] = (uint8_t)(reg_start);
					uart->TxBuff[4] = (uint8_t)(reg_num>>8U);
					uart->TxBuff[5] = (uint8_t)(reg_num);	
				// todo: reset error
					break;				
					
				
				
				// ================================ WRITE_SINGLE_COIL:
				case WRITE_SINGLE_COIL:					
					uart->TxMsgSize = 8;					
					uart->TxBuff[2] = (uint8_t)(reg_start>>8U);
					uart->TxBuff[3] = (uint8_t)(reg_start);
					uart->TxBuff[4] = (uint8_t)(*data>>8U);
					uart->TxBuff[5] = (uint8_t)(*data);	
					break;	
					
				// ================================ WRITE_MULTIPLE_REGISTERS:
				case WRITE_MULTIPLE_REGISTERS:
					// Check the number of registers to be written:
					if(reg_num>123)
						return -3;
					uart->TxMsgSize = 9+reg_num*2;					
					uart->TxBuff[2] = (uint8_t)(reg_start>>8U);
					uart->TxBuff[3] = (uint8_t)(reg_start);							
					uart->TxBuff[4] = (uint8_t)(reg_num>>8U);
					uart->TxBuff[5] = (uint8_t)(reg_num);	
					uart->TxBuff[6] = reg_num*2;
					for(uint8_t i=0; i<reg_num; i++)
					{
						uart->TxBuff[7+i*2] = (uint8_t)((data[i])>>8U);
						uart->TxBuff[8+i*2] = (uint8_t)(data[i]);	
					}
					break;				
									
				
				default:
					res = -1;
			}
			uint16_t crc;
			crc = ModbusCRC(uart->TxBuff, uart->TxMsgSize-2);
			uart->TxBuff[uart->TxMsgSize-2] = (uint8_t)(crc);
			uart->TxBuff[uart->TxMsgSize-1] = (uint8_t)(crc>>8U);		
			stage = MODBUS_GET_ANSWER;
			uart->MsgReceived = false;
			uart->Stage = SEND;
			break;
			
			
			
			
			
		// Wait for answer and parse it ==================================:	
		case MODBUS_GET_ANSWER:
			if(uart->MsgReceived)
			{
				uart->MsgReceived = false;
				// Errors and exeptions ==================:
				crc = ModbusCRC(uart->RxMessage, uart->RxMsgSize-2);
				if(uart->RxMessage[uart->RxMsgSize-2] != (uint8_t)(crc) ||	uart->RxMessage[uart->RxMsgSize-1] != (uint8_t)(crc>>8U))
				{
					res = -1;
				}
				if(uart->RxMessage[0] != device_addr)
				{
					res = -1;
				}
				if(uart->RxMessage[1] != (uint8_t)func)
				{
					res = -1;
				}
				if(res == 0)
				{
					// Parse message =========================:
					switch(func)
					{
						// ========================================== READ_COILS:
						case READ_COILS:
							for(uint8_t i=0; i<reg_num; i+=8)
							{							
								// todo: check this mechanism
								*((uint8_t*)data+i/8) = uart->RxMessage[3+i/8];
							}
							res = 1;
							break;
							
						// ================================= READ_DISCRETE_INPUTS:
						case READ_DISCRETE_INPUTS:
							for(uint8_t i=0; i<reg_num; i+=8)
							{							
								// todo: check this mechanism
								*((uint8_t*)data+i/8) = uart->RxMessage[3+i/8];
							}
							res = 1;
							break;
				
						// ================================ READ_HOLDING_REGISTERS:
						case READ_HOLDING_REGISTERS:
							for(uint8_t i=0; i<reg_num; i++)
							{
								data[i] = (uint16_t)uart->RxMessage[3+i*2]<<8U | (uint16_t)uart->RxMessage[3+i*2+1];
							}
							res = 1;
							break;	
							
						
						// ================================ READ_INPUT_REGISTERS:
						case READ_INPUT_REGISTERS:
							for(uint8_t i=0; i<reg_num; i++)
							{
								data[i] = (uint16_t)uart->RxMessage[3+i*2]<<8U | (uint16_t)uart->RxMessage[3+i*2+1];
							}
							res = 1;
							break;	
					
						// ================================ WRITE_SINGLE_COIL:
						case WRITE_SINGLE_COIL:
							stage = MODBUS_FORM_REQUEST;
							if(*data != (((uint16_t)uart->RxMessage[4])<<8U | (uint16_t)uart->RxMessage[5]))
								res = -1;
							else
								res = 1;
							break;
						
						// ================================ WRITE_MULTIPLE_REGISTERS:
						case WRITE_MULTIPLE_REGISTERS:						
							stage = MODBUS_FORM_REQUEST;
							if(reg_num != ((uint16_t)uart->RxMessage[4]<<8U | (uint16_t)uart->RxMessage[5]))
								res = -1;
							else
								res = 1;
							break;
							
						default:
							stage = MODBUS_FORM_REQUEST;
							res = -1;
							break;
					}
				}
			}
			// Wait for message ===============================:
			else		
				res = 0;
	}	
	if(res != 0)
	{	
		stage = MODBUS_FORM_REQUEST;
		cycles_timer = 0;
		return res;
	}
	return 0;
}	
	

/**********************************
  *	&		@brief  High level function for ModbusAsk().
  * & 	@param  ...
  * & 	@retval None
  *********************************/
void doModbus(tModbusFuncCode func, uint16_t reg_start, uint8_t reg_num, uint16_t* data)
{
	int res = 0;
	while(1)
	{
		res = ModbusAsk(func, MODBUS_MAIN_CPU_ADDRES, reg_start, reg_num, data, &gUart, 300);
		if(res>0)
			return;
		else if(res<0)
		{
			gModbusError = true;
			return;
		}
		osDelay(1);
	}
}




///**********************************
//  *	&	@brief  Add new connection between modbus register
//  *				and RAM value.
//  * & 	@param  *source - pointer to value in RAM;
//  * 			index   - number of register;
//  * & 	@retval None
//  *********************************/
//void CreateModbusInputRegTag16(uint16_t *source, uint16_t index)
//{
//	ModbusInutRegister[index].Value = source;
//}


/**********************************
  *	&	@brief  Compute the MODBUS RTU CRC.
  * & 	@param  *buf - pointer to message;
  * 			len  - length of message;
  * & 	@retval 16bit crc code.
  *********************************/
uint16_t ModbusCRC(uint8_t *buf, uint16_t len)
{
	uint16_t crc = 0xFFFF;

  for (uint16_t pos = 0; pos < len; pos++) {
    crc ^= (uint16_t)buf[pos];          // XOR byte into least sig. byte of crc

    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
    }
  }
  // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
  return crc;
}


// --------------------------------------------------------- Private functions -------------------:
///***********************
// *		@brief  Return value saved in Modbus Register.
// *  	@param  Modbus Register.
// *  	@retval Value.
// **********************/
//static uint16_t getValue16(tUint16Tag modbus_register)
//{
//	return (*(modbus_register.Value));
//}





/**********************************************************************************END OF FILE****/
