/**
  ************************************************************************************************
  * @file	 	 uart.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    19-January-2017
  * @brief   Header for uart.h.
  * 		 			
  *************************************************************************************************
  */
#ifndef UART_H
#define UART_H
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "cmsis_os.h"
#include "ApplicationCommon.h"

// -------------------------------------------------------- Exported define ----------------------:
// Time ===============:
#define UART_START_DELAY_MS			2000
#define UART_PERIOD_MS					1			//in ms
#define UART_TX_TIMEOUT_MS			2000

// Task ===============:
#define STACKSIZE_DISC_CPU_TASK	128
#define	PRIORITY__DISC_CPU_TASK	4

// UART structure =====:
#define UART_RX_MAX_SIZE			300
#define UART_TX_MAX_SIZE			300

// Tx Message sizes ===:
#define UART_GET_IP_MSG_SIZE		6
#define UART_RESET_IP_MSG_SIZE	18

// -------------------------------------------------------- Exported types -----------------------: 
typedef enum
{
	UART_CLIENT,
	UART_SERVER
}	tUartMode;

typedef enum
{
	SEND,								// Sending message. 
	WAIT_TX,						// Wait for end of message transfer. Turn ON Receive process.
	RECEIVE,						// Wait for Rx message.
	WAIT_RX,
	NONE								// Wait for new commands.
}	tUartStage;

// Command types ================:
typedef enum
{
	UART_RESET_IP,						// MainCPU should receive new IP from DiscoveryCPU and reset network settings.
	UART_GET_IP,							// MainCPU should send its IP to DiscoveryCPU.
	UART_UNCKNOWN_COMMAND
} tUartCommand;				// commands. 

// Uart structure ===============:
typedef struct
{
	tUartMode			Mode;
	tUartStage		Stage;
	uint8_t				RxBuff[UART_RX_MAX_SIZE];	
	uint8_t				RxMessage[UART_RX_MAX_SIZE];
	uint8_t				TxBuff[UART_TX_MAX_SIZE];
	int     			RxMsgSize;
	uint16_t			TxMsgSize;
	tUartCommand	Command;				// Command to be sent
	bool					Ready;					// Is uart ready for new Tx
	bool 					MsgReceived;		// Setted in case of input message.
}	tUart;

typedef enum
{
	UART_OK 	,		// No errors. 
	UART_TX_ERR	 	,		// 
	UART_RX_ERR		,   	// 
	UART_CRC_ERR,							// Wrong CRC
	UART_PARSING_ERR,   			// Wrong code "DISC". 
	UART_WRONG_ANSWER_ERR,
	UART_UNCKNOWN_COMMAND_ERR,	// MainCPU received wrong command.
	TIMEOUT_ERR,		// 
} tUART_Error;

 
// -------------------------------------------------------- Private macros -----------------------:
// Macroses:
#define SEND_NETWORK_SETT(UART)			\
		(UART).Command = UART_RESET_IP;	\
		(UART).Stage = SEND;						\
		gUART_Error = UART_OK

#define GET_NETWORK_SETT(UART)			\
		(UART).Command = UART_GET_IP;		\
		(UART).Stage = SEND;						\
		gUART_Error = UART_OK 


// -------------------------------------------------------- Exported variables -------------------: 
extern tUart gUart;
extern tUART_Error	gUART_Error;
 
// -------------------------------------------------------- Exported functions -------------------: 
extern uint8_t	UartInit(void);
extern void 		UART_Thread(void const *argument);
 
// -------------------------------------------------------- Exported constants -------------------: 





/////////////////////
// UART
/////////////////////

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* User can use this section to tailor USARTx/UARTx instance used and associated 
   resources */
/* Definition for USARTx clock resources */
#define USARTx                           USART6
#define USARTx_CLK_ENABLE()              __USART6_CLK_ENABLE()
#define DMAx_CLK_ENABLE()                __HAL_RCC_DMA2_CLK_ENABLE()
#define USARTx_RX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __USART6_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __USART6_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_6
#define USARTx_TX_GPIO_PORT              GPIOC
#define USARTx_TX_AF                     GPIO_AF8_USART6
#define USARTx_RX_PIN                    GPIO_PIN_7
#define USARTx_RX_GPIO_PORT              GPIOC
#define USARTx_RX_AF                     GPIO_AF8_USART6

/* Definition for USARTx's DMA */
#define USARTx_TX_DMA_STREAM              DMA2_Stream6
#define USARTx_RX_DMA_STREAM              DMA2_Stream1
#define USARTx_TX_DMA_CHANNEL             DMA_CHANNEL_5
#define USARTx_RX_DMA_CHANNEL             DMA_CHANNEL_5

/* Definition for USARTx's NVIC */
#define USARTx_DMA_TX_IRQn                DMA2_Stream6_IRQn
#define USARTx_DMA_RX_IRQn                DMA2_Stream1_IRQn
#define USARTx_DMA_TX_IRQHandler          DMA2_Stream6_IRQHandler
#define USARTx_DMA_RX_IRQHandler          DMA2_Stream1_IRQHandler

/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART6_IRQn
#define USARTx_IRQHandler                USART6_IRQHandler

/////////////////////
// UART
/////////////////////


#endif

