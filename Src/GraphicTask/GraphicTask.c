/**
  *************************************************************************************************
  * @file	 	 GraphicTask.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    11-January-2017
  * @brief   Here is Graphic Thread function.
  *************************************************************************************************
  */


// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include <math.h>	
#include <stdlib.h>	
#include "GraphicTask/GraphicCommon.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/MainWindow.h"
#include "GraphicTask/SettingsWindow.h"
#include "Uart/Modbus.h"

/* FatFs includes component */
#include "ff_gen_drv.h"
#include "sd_diskio.h"

/* Jpeg includes component */
#include <stdint.h>
#include <string.h>

// --------------------------------------------------------- Global variables --------------------:

// --------------------------------------------------------- Private variables -------------------:
static uint8_t 		touched_prev;
static uint16_t   errorPointTimer;

FATFS SDFatFs;  /* File system object for SD card logical drive */
FIL 	MyFile;     /* File object */
char 	SDPath[4]; /* SD card logical drive path */


// --------------------------------------------------------- Private function prototypes ---------:

// --------------------------------------------------------- Global functions --------------------:


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @GraphicThread
  *	&	  @brief  Draws windows on display, executes all buttons pressing.
  * & 	@param  *argument
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void GraphicThread(void const *argument)
{
	__IO TS_StateTypeDef  ts;  			// Touch screen state structure.
	
//	LCD_Config();
		
	// Wait for thermometers to be initialiazed (in I2C Thread) =========:
	osDelay(GRAPHIC_THREAD_DELAY);								
	// Initializes and configures the touch screen functionalities ======:
	BSP_TS_Init(420, 272);		// 420x272 pixels.
	
	// Set background color and fill display ====================:
	GUI_SetBkColor(COLOR_BK1);
  GUI_Clear();
	// All graphics will be drawn on 0 layer ====================:
//	LCD_Config();	
	
//	 /*##-2- Link the micro SD disk I/O driver ##################################*/
//  if(FATFS_LinkDriver(&SD_Driver, SDPath) == 0)
//  {
//    /*##-3- Register the file system object to the FatFs module ##############*/
//    if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) == FR_OK)
//    {
//      /*##-4- Open the JPG image with read access ############################*/
//       if(f_open(&MyFile, "BkBlue.gif", FA_READ) == FR_OK)
//       {
//				 __NOP();
//       }
//			 else
//				 __NOP();
//     }
//  }	
////	void* gifBuffer = malloc(102400);
////	f_read(&MyFile, gifBuffer, 102400, (UINT *)&BytesRead);
//	GUI_GIF_DrawEx(APP_GetData, &MyFile, 0, 0); 
////	free(gifBuffer); 
//	
//	/*##-6- Close the JPG image with read access ############################*/
//	if(f_close(&MyFile) == FR_OK)
//	{
//		__NOP();
//	}	
	GUI_SelectLayer(0);	
	
		
	InitializeScreenSectors();
	// Initialize pararmeters of all windows and keyboard =======:
	MainWin_Init();
	SettingsWin_Init();	
	KeyBoard_Init();
		
	// Start with Main Window ===================================:
	gActiveWindow = MAIN_WIN;
	
	// Scrin hasn't been touched ================================: 
	touched_prev = 0;
	errorPointTimer = 0;
	
  while(1)
	{
		// Get touch screen parameters ============================:
		BSP_TS_GetState((TS_StateTypeDef *)&ts);
		uint8_t touchDetected = ts.touchDetected;		
		
		// Go to Active Window Process ============================:		
		switch(gActiveWindow)				
		{
			case SETTINGS_WIN:
				SettingsWin_Process(touched_prev, ts);
				break;
			case MAIN_WIN:
			default:
				MainWin_Process(touched_prev, ts);
				break;
		}
			
		// Draw point in right up corner in case of Modbus error ========:
		if(gModbusError)
		{
			GUI_SetColor(GUI_BLACK);
			GUI_FillRect(476,2,478,4);
			if(errorPointTimer++ >= ERROR_POINT_TIME_MS/GRAPHIC_PERIOD_MS)
			{
				GUI_SetColor(COLOR_BK1);
				GUI_FillRect(476,2,478,4);
				errorPointTimer=0;
				gModbusError = false;
			}
		}		
				
		touched_prev = touchDetected;
			
		osDelay(GRAPHIC_PERIOD_MS);
	}
}



// --------------------------------------------------------- Private functions -------------------:



/**********************************************************************************END OF FILE****/
