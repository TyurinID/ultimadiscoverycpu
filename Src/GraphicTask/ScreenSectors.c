/**
  ************************************************************************************************
  * @file		 ScreenSectors.c
  * @author  Tyurin Ivan
  * @version V2.0
  * @date    13-January-2016
  * @brief   File with functions Sreen sectors.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------: 
#include "GraphicTask/ScreenSectors.h"


// --------------------------------------------------------- Global variables --------------------:
TScreenSector GScreen_sector[SCREEN_SECTORS_NUM];
uint16_t gCurrentDisplayOffset;											// Coordinate in virtual display space
																										// from which drawing starts.

// --------------------------------------------------------- Private variables -------------------:  
  
// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:


void InitializeScreenSectors(void)
{
	GScreen_sector[0].FXBegin = 0;
	GScreen_sector[0].FXEnd   = 122;
	GScreen_sector[0].FYBegin = 0;  
	GScreen_sector[0].FYEnd 	= 270;
	
	GScreen_sector[1].FXBegin = 118;
	GScreen_sector[1].FXEnd   = 242;
	GScreen_sector[1].FYBegin = 0;  
	GScreen_sector[1].FYEnd 	= 270;

	GScreen_sector[2].FXBegin = 238;
	GScreen_sector[2].FXEnd   = 362;
	GScreen_sector[2].FYBegin = 0;  
	GScreen_sector[2].FYEnd 	= 270;	
	
	GScreen_sector[3].FXBegin = 358;
	GScreen_sector[3].FXEnd   = 480;
	GScreen_sector[3].FYBegin = 0;  
	GScreen_sector[3].FYEnd 	= 270;

	gCurrentDisplayOffset = 0;
}

void DrawBitmapInScreenSector(const GUI_BITMAP * pBM, int x0, int y0, uint16_t offset, TScreenSector* screen_sector) 
{
	x0 -= (int)offset;
	GUI_DrawBitmap(pBM, x0  , y0);
}

void DrawLineInScreenSector(int x1, int y1, int x2, int y2, uint16_t offset, TScreenSector* screen_sector)
{
		
		x1 -= (int)offset;
		x2 -= (int)offset;
//		if(x1 < 0 || x2 < 0)
//			return;
		uint8_t do_draw = 0;
		if(x1 == x2)	// VERTICAL line
		{
			if( (x1>=screen_sector->FXBegin && x1 <=screen_sector->FXEnd) 
				|| (x2>=screen_sector->FXBegin && x2 <=screen_sector->FXEnd ) )
			{
				do_draw = 1;
			}
		}
		else
		{
			if(y1 == y2) // HORIZONTAL line
			{
				if( (y1>=screen_sector->FYBegin && y1 <=screen_sector->FYEnd)
				||  (y2>=screen_sector->FYBegin && y2 <=screen_sector->FYEnd) )
				{
					do_draw = 1;
				}
			}
			else
			{
								// diagonal line
			
				if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
							(screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
						( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
							(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
				{
					do_draw =1;
				}
			}
		}
		
		if(do_draw)
		{
			GUI_DrawLine(x1, y1, x2, y2);
		}		
}


void DrawRectInScreenSector(GUI_RECT pRect, uint16_t offset, TScreenSector* screen_sector)
{
	GUI_RECT Rect = pRect;
	Rect.x0 -= (int)offset;
	Rect.x1 -= (int)offset;	
//	if(Rect.x0 < 0 || Rect.x1 < 0)
//		return;
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (Rect.x0 >= screen_sector->FXBegin && Rect.x0<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= Rect.x0 && screen_sector->FXBegin <= Rect.x1) ) &&
	    ( (Rect.y0 >= screen_sector->FYBegin && Rect.y0<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= Rect.y0 && screen_sector->FYBegin <= Rect.y1) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_DrawRectEx(&Rect);
	}
}

void FillRectInScreenSector(GUI_RECT pRect, uint16_t offset, TScreenSector* screen_sector)
{
	GUI_RECT Rect = pRect;
	Rect.x0 -= (int)offset;
	Rect.x1 -= (int)offset;	
//	if(Rect.x0 < 0 || Rect.x1 < 0)
//		return;
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (Rect.x0 >= screen_sector->FXBegin && Rect.x0<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= Rect.x0 && screen_sector->FXBegin <= Rect.x1) ) &&
	    ( (Rect.y0 >= screen_sector->FYBegin && Rect.y0<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= Rect.y0 && screen_sector->FYBegin <= Rect.y1) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_FillRectEx(&Rect);
	}
}


void DrawTextInScreenSector(const char *s, int x1, int y1, uint16_t offset, TScreenSector* screen_sector)
{
	
	x1 -= (int)offset;
//	if(x1 < 0)
//		return;
	
	int x2 = x1+GUI_GetStringDistX(s);
	int y2 = y1+GUI_GetFontSizeY();
	
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
	    ( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_DispStringAt(s, x1, y1);
	}
}

void DrawRectTextInScreenSector(const char *s, GUI_RECT pRect, int TextAlign,  uint16_t offset, TScreenSector* screen_sector)
{
	// check do_draw
	uint8_t do_draw = 0;
	GUI_RECT Rect = pRect;
	Rect.x0 -= (int)offset;
	Rect.x1 -= (int)offset;
//	if(Rect.x0 < 0 || Rect.x1 < 0)
//		return;
	
	if( ( (Rect.x0 >= screen_sector->FXBegin && Rect.x0<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= Rect.x0 && screen_sector->FXBegin <= Rect.x1) ) &&
	    ( (Rect.y0 >= screen_sector->FYBegin && Rect.y0<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= Rect.y0 && screen_sector->FYBegin <= Rect.y1) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_DispStringInRectWrap(s, &Rect, TextAlign, GUI_WRAPMODE_WORD);
	}
}
