/**
  *************************************************************************************************
  * @file	 	 GraphicCommon.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    11-January-2017
  * @brief   Here is Graphic Common functions.
  *************************************************************************************************
  */
	
#include "GraphicTask/GraphicCommon.h"

tActiveWindow gActiveWindow; 						// Type of current window to be displaed.

GUI_MEMDEV_Handle hMem;

