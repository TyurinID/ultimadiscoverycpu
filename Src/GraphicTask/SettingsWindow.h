
#ifndef SETTINGS_WINDOW
#define SETTINGS_WINDOW

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include <math.h>
#include "GraphicTask/GraphicTask.h"

#include "GraphicTask/ScreenSectors.h"

#include <string.h>

#include "GraphicTask/GraphicCommon.h"

#include "GraphicTask/GraphKeyBoard.h"

#include "GraphicTask/NetworkDlg.h"

#include "GraphicTask/MainWindow.h"
#include "Uart/uart.h"

extern uint8_t GToDrawFLG;
extern uint8_t win_number;

// Settings works in reaction mode. Something changes only if display is touched.
extern void SettingsWin_Init(void);
extern void SettingsWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts);

// Buttons coordinates ====:
#define		BUTTON_LEFT_X0	3
#define		BUTTON_LEFT_X1	117

#define		BUTTON_RIGHT_X0	361
#define		BUTTON_RIGHT_X1	475

// Fields coordinates =====:
#define		FIELD_BIG_X0		60
#define		FIELD_BIG_X1		180
#define		FIELD_X0				70
#define		FIELD_X1				170

#define		FIELD1_BIG_Y0		20
#define		FIELD1_BIG_Y1		100
#define		FIELD1_Y0				70
#define		FIELD1_Y1				95

#define		FIELD2_BIG_Y0		120
#define		FIELD2_BIG_Y1		200
#define		FIELD2_Y0				170
#define		FIELD2_Y1				195

#endif
