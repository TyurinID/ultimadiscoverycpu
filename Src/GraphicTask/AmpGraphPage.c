/**
  ************************************************************************************************
  * @file	   AmpGraphPage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include "GraphicTask/AmpGraphPage.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/ScreenSectors.h"
#include <math.h>


	
// --------------------------------------------------------- Global variables --------------------:
tGraph gAmpGraph;					// Structure for Amp Graphic.
 
// --------------------------------------------------------- Private variables -------------------:  
	
	
// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:

  //GUI_Clear();
//	GUI_RECT Rect;
//	GUI_SetTextMode(GUI_TM_TRANS);
//	GUI_SetColor(COLOR_DARK);
//	GUI_SetFont(GUI_FONT_24_ASCII);
	
	
 /***********************
  *		@brief  Draw all elements of AmpGraphPage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Draw_AmpGraphPage(TScreenSector* sector)
{
	DrawGraph(gAmpGraph, gCurrentDisplayOffset, sector);
}

 /***********************
  *		@brief  Initialize all elements of Amp Graph Page.
  *  	@param  None
  *  	@retval None
  **********************/
void	Init_AmpGraphPage(void)
{
	// Configure gAmpGraph =========:
	gAmpGraph.PointsNum = 220;
	for(uint8_t i=0;i<gAmpGraph.PointsNum;i++)
	{
		gAmpGraph.xValue[i] = i*7 - 300.0; 
		gAmpGraph.yValue[i] = 25*sin((float)i/20);
	}	
	
	
	gAmpGraph.x0 = GRAPH_X0 + AMPGRAPH_PAGE_OFFSET;
	gAmpGraph.x1 = GRAPH_X1 + AMPGRAPH_PAGE_OFFSET;
	gAmpGraph.y0 = GRAPH_Y0;
	gAmpGraph.y1 = GRAPH_Y1;	
	gAmpGraph.NetON = 1;
	gAmpGraph.Color.Axis = COLOR_GRAPH_SCALES;
	gAmpGraph.Color.Curve = GUI_RED;
	gAmpGraph.Color.Names = COLOR_GRAPH_SCALES;
	gAmpGraph.Color.Net = COLOR_GRAPH_NET;
	
	gAmpGraph.Indent.xLeft = 0;
	gAmpGraph.Indent.xRight = 0;
	gAmpGraph.Indent.yBottom = 0;
	gAmpGraph.Indent.yTop = 0;
		
	sprintf(gAmpGraph.xName, "Time, us");	
	sprintf(gAmpGraph.yName, "Amp");	
	
	InitializeGraph(&gAmpGraph);
}


// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
