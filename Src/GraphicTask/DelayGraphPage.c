/**
  ************************************************************************************************
  * @file	   DelayGraphPage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include	"GraphicTask/DelayGraphPage.h"
#include	"GraphicTask/GraphicCommon.h"
#include	<math.h>



// --------------------------------------------------------- Global variables --------------------:
tGraph 	gDelayGraph;					// Structure for Delay Graphic.
float		gDelayValue;
// --------------------------------------------------------- Private variables -------------------: 
  
// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:

  //GUI_Clear();
//	GUI_RECT Rect;
//	GUI_SetTextMode(GUI_TM_TRANS);
//	GUI_SetColor(COLOR_DARK);
//	GUI_SetFont(GUI_FONT_24_ASCII);
	
	
 /***********************
  *		@brief  Draw all elements of DelayGraphPage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Draw_DelayGraphPage(TScreenSector* sector)
{
	DrawGraph(gDelayGraph, gCurrentDisplayOffset, sector);
	
	GUI_RECT Rect;
	char str[16] = "";
	GUI_SetFont(GUI_FONT_24_1);
	GUI_SetColor(COLOR_BK2);	
	Rect.x0 = DELAY_VALUE_X0 + DELAYGRAPH_PAGE_OFFSET;
	Rect.x1 = DELAY_VALUE_X1 + DELAYGRAPH_PAGE_OFFSET;
	Rect.y0 = DELAY_VALUE_Y0;	
	Rect.y1 = DELAY_VALUE_Y1;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
	GUI_SetColor(COLOR_WHITE);	
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_SetColor(COLOR_WHITE);		
	sprintf(str, "Delay\n%g us", gDelayValue);
	DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
}

 /***********************
  *		@brief  Initialize all elements of DelayGraphPage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Init_DelayGraphPage(void)
{
	// Configure gDelayGraph =========:
	gDelayGraph.PointsNum = 220;
	for(uint8_t i=0;i<gDelayGraph.PointsNum;i++)
	{
		gDelayGraph.xValue[i] = i*7; 
		gDelayGraph.yValue[i] = 7*sin((float)i/20);
	}	
	
	gDelayGraph.x0 = GRAPH_X0 + DELAYGRAPH_PAGE_OFFSET;
	gDelayGraph.x1 = GRAPH_X1 + DELAYGRAPH_PAGE_OFFSET;
	gDelayGraph.y0 = GRAPH_Y0;
	gDelayGraph.y1 = GRAPH_Y1;	
	gDelayGraph.NetON = 1;
	gDelayGraph.Color.Axis = COLOR_GRAPH_SCALES;
	gDelayGraph.Color.Curve = GUI_RED;
	gDelayGraph.Color.Names = COLOR_GRAPH_SCALES;
	gDelayGraph.Color.Net = COLOR_GRAPH_NET;
	gDelayGraph.Color.Bk = COLOR_BK1;
	
	gDelayGraph.Indent.xLeft = 0;
	gDelayGraph.Indent.xRight = 0;
	gDelayGraph.Indent.yBottom = 0;
	gDelayGraph.Indent.yTop = 0;
		
	sprintf(gDelayGraph.xName, "Time, us");	
	sprintf(gDelayGraph.yName, "Voltage, V");	
	
	gDelayGraph.xMarkON = 0;
//	gDelayGraph.xMarkValue = 422;	
//	gDelayGraph.xMarkAlignment = MARK_RIGHT;
//	sprintf(gDelayGraph.xMarkSignature, "%g us", 422.0);
	
	InitializeGraph(&gDelayGraph);	
	
	gDelayValue = 233;
}


// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
