/**
  ************************************************************************************************
  * @file	   MeasurePage.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   
  *************************************************************************************************
  */
#ifndef MEASURE_PAGE_H
#define MEASURE_PAGE_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"
#include "GraphicTask/Graph.h"


// -------------------------------------------------------- Exported define ----------------------:
// Current page offset =======:
#define		MEASURE_PAGE_OFFSET	2400		// x coordinate in virtual display space.

#define 	INJECTORS_NUM					1

#define		MAX_PERCENT_BORDER			0.9f
#define		MIN_PERCENT_BORDER			0.4f

// Coordinates =======:
#define		BK_LINE_0_Y0						5
#define		BK_LINE_0_Y1						30
#define   BK_LINE_1_Y0						35
#define   BK_LINE_1_Y1						145
#define   BK_LINE_2_Y0						155
#define   BK_LINE_2_Y1						265
#define   BK_TEXT_HEIGHT					22
#define		GAUGES_X0								20
#define		GAUGES_INDUCTANCE_Y0		140
#define		GAUGES_RESISTANCE_Y0		260
#define		GAUGES_X_SHIFT					120

// Gauge =========:
#define 	GAUGE_LINE_HEIGHT				20
#define		GAUGE_LINE_WIDTH				80
#define		GAUGE_NUMBER_HEIGHT			60

#define		GAUGE_COIL_INDUCTANCE_TOP			500
#define		GAUGE_COIL_RESISTANCE_TOP			3
#define		GAUGE_PIEZO_CAPACITANCE_TOP		10
#define		GAUGE_PIEZO_RESISTANCE_TOP		300



// -------------------------------------------------------- Exported types -----------------------: 
typedef	enum
{
	INJ_COIL,
	INJ_PIEZO,
	INJ_DUMMY = 0xffff
}	tInjType;

typedef struct
{
	float			Param1[INJECTORS_NUM];
	float			Param2[INJECTORS_NUM];
	
	tInjType	InjType;
}	tMeasured; 
 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern tMeasured gMeasured;					// Structure for Measured Data.
 
 
// -------------------------------------------------------- Exported functions -------------------: 
extern void	Draw_MeasurePage(TScreenSector* sector);
extern void	Init_MeasurePage(void);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
