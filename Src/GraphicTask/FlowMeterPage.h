/**
  ************************************************************************************************
  * @file	   FlowMeterPage.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
#ifndef FLOW_METER_PAGE_H
#define FLOW_METER_PAGE_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"
#include	"GraphicTask/ScreenSectors.h"


// -------------------------------------------------------- Exported define ----------------------:
// Current page offset =======:
#define		FLOWMETER_PAGE_OFFSET	960		// x coordinate in virtual display space.

// Defines for flowmeter:
#define 	CHANNEL_NUM		4

//// Buttons coordinates ====:
//#define		BUTTON_Y0			228
//#define		BUTTON_Y1			267

//#define		BUTTON1_X0			3
//#define		BUTTON1_X1			127

//#define		BUTTON2_X0			122
//#define		BUTTON2_X1			236

//#define		BUTTON3_X0			351
//#define		BUTTON3_X1			475

// Flasks =================:
#define 	FLASKS_SHIFT		117
#define		FLASK_HEIGHT		155
#define		FLASK_WIDTH			48

// Labels coordinates =====:
#define 	LABEL_Y0				2
#define 	LABEL_Y1				24
#define 	LABEL1_X0				15
#define 	LABEL1_X1				231

#define 	LABEL2_X0				249
#define 	LABEL2_X1				465

#define 	LABEL_DOWN_Y0		243
#define 	LABEL_DOWN_Y1		268

// Taho coordinates =======:
#define 	TAHO_X0					345
#define 	TAHO_X1					402
#define 	TAHO_Y0					246
#define 	TAHO_Y1					265
  
// -------------------------------------------------------- Exported types -----------------------: 
// Structure for all data transmitted from flow meter:
typedef struct
 {
	float			Value;
	float			Temperature1;
	float			Temperature2;
	 
	char 			*Name;
 }	tChannel;

typedef struct
 {
	uint16_t	Taho;
	tChannel	Channel[CHANNEL_NUM];
	float			TopValue;
	bool			SecondThermometersON;
 }	tFlowMeter;
 
 
 
 
 // For Flasks drawing ================:
 typedef enum
{
  FLASK_1TH  = 0, 
  FLASK_2TH
} TFlaskMode;

typedef enum
{
  NOTHING  = 0, 
  NORMAL,
	DOUBLE,
	TRIPPLE
} TWidth;
 
 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern tFlowMeter 	gFlowMeter;
 
 
// -------------------------------------------------------- Exported functions -------------------: 
extern void	Draw_FlowMeterPage(TScreenSector* sector);
extern void	Init_FlowMeterPage(void);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
