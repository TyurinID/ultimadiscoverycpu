/**
  ************************************************************************************************
  * @file	 	 Graph.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    12-January-2017
  * @brief   How to use Graph:
	*							(-)	Configure structure tGraph: 
	*									(++)	pointers to xValues and yValues [Value range should be more than 5 and less than 10 000];
	*									(++)	coordinates for displaying Graph on screen, colors, axises names;
	*									(++)	turn on/off net and indents.	
	*							(-)	Initialize configured Graph by InitializeGraph() function.
	*							(-)	Draw graph on display by function DrawGraph();
  *************************************************************************************************
  */
#ifndef GRAPH_H
#define GRAPH_H


// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"

#include "GraphicTask/ScreenSectors.h"

// -------------------------------------------------------- Exported define ----------------------:
// List with steps values =============:
#define   GRAPH_STEPS_NUM				40		// 

#define 	GRAPH_AXIS_NAME_MAX_LENGTH	20

#define   GRAPH_POINTS_NUM_MAX			256

// For Graph drawing ===============================:
#define 	GRAPH_AXIS_WIDTH			2			// in pxls
#define 	GRAPH_X_AUTO_INDENT		5		// in pxls
#define 	GRAPH_Y_AUTO_INDENT		5		// in pxls

#define 	GRAPH_NUMBERS_INDENT	3			// in pxls
#define		GRAPH_Y_NAME_X_INDENT	0			// in pxls
#define		GRAPH_Y_NAME_Y_INDENT	20		// in pxls
#define		GRAPH_X_NAME_X_INDENT	0			// in pxls
#define		GRAPH_X_NAME_Y_INDENT	1			// in pxls

#define   GRAPH_X_NETLINES_NUM	8
#define   GRAPH_Y_NETLINES_NUM	6			
#define   GRAPH_X_NETLINES_MAX_NUM	GRAPH_X_NETLINES_NUM + 20
#define   GRAPH_Y_NETLINES_MAX_NUM	GRAPH_Y_NETLINES_NUM + 20	

	
// -------------------------------------------------------- Exported types -----------------------: 
typedef struct
{
	uint32_t			Bk;
	uint32_t			Axis;
	uint32_t			Curve;
	uint32_t			Names;
	uint32_t			Net;
}	tGraphColors;

typedef struct
{
	bool					xLeft;
	bool					xRight;
	bool					yTop;
	bool					yBottom;
}	tIndent;

typedef enum
{
	MARK_LEFT,
	MARK_RIGHT
}	tMark;

// Structure for graphics ================================:
typedef struct
{
	// Parameters to be configured before executing InitializeGraph():
	int						x0;
	int						y0;
	int						x1;
	int						y1;
	char					xName[GRAPH_AXIS_NAME_MAX_LENGTH];
	char					yName[GRAPH_AXIS_NAME_MAX_LENGTH];
	tGraphColors	Color;
	
	float					xValue[GRAPH_POINTS_NUM_MAX];
	float					yValue[GRAPH_POINTS_NUM_MAX];
	uint16_t			PointsNum;
	
	bool					NetON;
	tIndent				Indent;
	
	bool					xMarkON;
	tMark					xMarkAlignment;
	float					xMarkValue;
	char					xMarkSignature[16];
	
	// Parameters configured by InitializeGraph() function:
	uint8_t				xNetLinesNum;
	uint8_t				yNetLinesNum;
	float					xNetLineValue[GRAPH_X_NETLINES_MAX_NUM];
	float					yNetLineValue[GRAPH_Y_NETLINES_MAX_NUM];
	
	float					xMinValue;
	float					yMinValue;
	float					xCoeff;
	float					yCoeff;
	int						xStartCoord;
	int						yStartCoord;
	
}	tGraph; 
  
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
 
// -------------------------------------------------------- Exported functions -------------------: 
extern int InitializeGraph(tGraph* graph); 
extern void	DrawGraph(tGraph graph, uint16_t offset, TScreenSector* sector);

// -------------------------------------------------------- Exported constants -------------------: 
 
 #endif
 
/**********************************************************************************END OF FILE****/ 

