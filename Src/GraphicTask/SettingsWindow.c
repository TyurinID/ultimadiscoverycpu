#include "GraphicTask/SettingsWindow.h"
#include "Uart/Modbus.h"
#include "Application/Application.h" 

extern tUart gUart;			// Main structure for Uart communication.
extern GUI_CONST_STORAGE GUI_BITMAP bmButtonYellow;

uint8_t GToDrawFLG;

static char settingsWinName[7][12];
static uint8_t touchCnt;
uint8_t win_number;

static void SettingsWin_Draw(void);

void SettingsWin_Init(void)
{
	win_number = 6;
	GToDrawFLG = 0;
}

void SettingsWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts)
{
	// Draw everything =================================================:
	if(GToDrawFLG)
	{
		GToDrawFLG = 0;
		if(GKeyBoard.Active)
			KeyBoard_Draw();
		else
			SettingsWin_Draw();
	}
	
	// Process everything ==============================================:
	if(ts.touchDetected == 1 && touched_prev == 0)
	{	
		if( BUTTON_Y0 < ts.touchY[0] )		// Buttons:
		{
			// Button "GET" ==========================================:
			if( ts.touchX[0] < BUTTON_LEFT_X1 )
			{
				// ============================ Read network settings from MAinCPU:
				gGetNetworkSett = true;
				osDelay(100);
				GToDrawFLG = 1;
			}
			// Button "SET" ==========================================:
			if( ts.touchX[0] > BUTTON_RIGHT_X0 )
			{
				// ================= Send network settings to MAinCPU and check it:
				gSetNetworkSett = true;
				osDelay(100);				
				GToDrawFLG = 1;
			}
			if ( BUTTON_LEFT_X1 < ts.touchX[0] && ts.touchX[0] < BUTTON_RIGHT_X0 ) 
			{
				if(++touchCnt == 5)
				{
					gActiveWindow = MAIN_WIN;		// Go to Main Window.
					touchCnt = 0;
				}
			}
		}
		else
		{
			if(GKeyBoard.Active)
				KeyBoard_Process(ts);
			else
				switch(win_number)
				{
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
						NetworkDlg_Process(ts);
						break;
				}
		}
	}	
}

static void SettingsWin_Draw(void)
{
	for(uint8_t i=0; i<SCREEN_SECTORS_NUM; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYBegin, 
														 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();	
		
		GUI_RECT Rect;
		GUI_SetTextMode(GUI_TM_TRANS);
		GUI_SetColor(COLOR_DARK);
		GUI_SetFont(GUI_FONT_24_ASCII);
		
		
		// Buttons and window_name ======================================:
		GUI_DrawBitmap(&bmButtonYellow, BUTTON_LEFT_X0, BUTTON_Y0);
		Rect.y0 = BUTTON_Y0;
		Rect.y1 = BUTTON_Y1;
		Rect.x0 = BUTTON_LEFT_X0;
		Rect.x1 = BUTTON_LEFT_X1;
		GUI_DispStringInRectWrap("GET", &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);

		GUI_DrawBitmap(&bmButtonYellow, BUTTON_RIGHT_X0, BUTTON_Y0);
		Rect.x0 = BUTTON_RIGHT_X0;
		Rect.x1 = BUTTON_RIGHT_X1;
		GUI_DispStringInRectWrap("SET", &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		
		Rect.x0 = BUTTON_LEFT_X1+5;
		Rect.x1 = BUTTON_RIGHT_X0-5;
		Rect.y0 = BUTTON_Y0+2;
		Rect.y1 = BUTTON_Y1-2;
		GUI_SetColor(COLOR_WHITE);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_DispStringInRectWrap(settingsWinName[win_number], &Rect, GUI_TA_VCENTER |
																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		GUI_SetColor(COLOR_DARK);
		GUI_DrawRectEx(&Rect);
		Rect.y0 = BUTTON_Y0+1;
		Rect.y1 = BUTTON_Y1-1;
		Rect.x0 = BUTTON_LEFT_X1+6;
		Rect.x1 = BUTTON_RIGHT_X0-6;
		GUI_DrawRectEx(&Rect);	
		
		// General field:
		switch(win_number)
		{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				NetworkDlg_Draw();
				break;
		}
				
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}

static char settingsWinName[7][12] = {
																"General",
																"Channel 1",
																"Channel 2",
																"Channel 3",
																"Channel 4",
																"Pump",
																"Network"};
