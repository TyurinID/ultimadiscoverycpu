/**
  ************************************************************************************************
  * @file	   FlowMeterPage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include	"GraphicTask/FlowMeterPage.h"
#include 	"GraphicTask/GraphicCommon.h"
#include	<math.h>

	
// --------------------------------------------------------- Global variables --------------------:
tFlowMeter 	gFlowMeter;
	
// --------------------------------------------------------- Private variables -------------------:
static const 	uint32_t GradientOrange[FLASK_WIDTH-1];
static char 	ChannelNames[CHANNEL_NUM][15];
  
// --------------------------------------------------------- Private function prototypes ---------:	
static void DrawFlask(tChannel channel, TFlaskMode mode, uint16_t x, uint16_t y, TWidth width_coeff, uint8_t flaskHeight, uint16_t offset, TScreenSector* sector);
static void DrawLabelsTaho(uint16_t offset, TScreenSector* sector);
static void GetTopValue(void);
	
// --------------------------------------------------------- Global functions --------------------:

  //GUI_Clear();
//	GUI_RECT Rect;
//	GUI_SetTextMode(GUI_TM_TRANS);
//	GUI_SetColor(COLOR_DARK);
//	GUI_SetFont(GUI_FONT_24_ASCII);
	
	
 /***********************
  *		@brief  Draw all elements of FlowMeterPage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Draw_FlowMeterPage(TScreenSector* sector)
{		
	static uint8_t taho_cnt = 0;		
	if(++taho_cnt == 4) 						// Calculate top value once each 4 iterations.
	{
		taho_cnt = 0;	
		GetTopValue();		
	}
	
	DrawLabelsTaho(gCurrentDisplayOffset, sector);							// Draw labels and taho.	
		
	for(uint8_t	j=0; j<CHANNEL_NUM; j++)
		if(gFlowMeter.SecondThermometersON)
			DrawFlask(gFlowMeter.Channel[j], FLASK_2TH, LABEL1_X0 + j*FLASKS_SHIFT + FLOWMETER_PAGE_OFFSET, 212, DOUBLE, FLASK_HEIGHT, gCurrentDisplayOffset, sector);
		else
			DrawFlask(gFlowMeter.Channel[j], FLASK_1TH, LABEL1_X0 + j*FLASKS_SHIFT + FLOWMETER_PAGE_OFFSET, 238, DOUBLE, FLASK_HEIGHT+20, gCurrentDisplayOffset, sector);		
}

 /***********************
  *		@brief  Initialize all elements of FlowMeterPage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Init_FlowMeterPage(void)
{
	for(uint8_t	i=0; i<CHANNEL_NUM; i++)
		gFlowMeter.Channel[i].Name = ChannelNames[i];
	gFlowMeter.SecondThermometersON = true;
	gFlowMeter.Taho = 300;
	gFlowMeter.Channel[0].Value = 45.5;
	gFlowMeter.Channel[1].Value = 25.89;
	gFlowMeter.Channel[2].Value = 10.0;
	gFlowMeter.Channel[3].Value = 33.3;
	
	gFlowMeter.Channel[0].Temperature1 = 21.5;
	gFlowMeter.Channel[1].Temperature1 = 22.89;
	gFlowMeter.Channel[2].Temperature1 = 23.0;
	gFlowMeter.Channel[3].Temperature1 = 24.3;	
	gFlowMeter.Channel[0].Temperature2 = 31.5;
	gFlowMeter.Channel[1].Temperature2 = 32.89;
	gFlowMeter.Channel[2].Temperature2 = 33.0;
	gFlowMeter.Channel[3].Temperature2 = 34.3;	
}


// --------------------------------------------------------- Private functions -------------------:
static void DrawFlask(tChannel channel, TFlaskMode mode, uint16_t x, uint16_t y, TWidth width_coeff, uint8_t flaskHeight, uint16_t offset, TScreenSector* sector)
{	
	float output_value;
	char str[5] ="";
	GUI_SetFont(GUI_FONT_20_1);
	GUI_RECT Rect;
	
	// draw flask borders:
	GUI_SetColor(GUI_BLACK);
	Rect.x0 = x;
	Rect.x1 = x+2;
	Rect.y0 = y-flaskHeight;
	Rect.y1 = y;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);	
	Rect.x0 = x+FLASK_WIDTH*width_coeff+1;
	Rect.x1 = x+FLASK_WIDTH*width_coeff+3;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);	
	Rect.x0 = x+3;
	Rect.x1 = x+FLASK_WIDTH*width_coeff;
	Rect.y0 = y-3;
	Rect.y1 = y;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);
		
	// draw backgound in flask: 
	uint16_t oil_level = (uint16_t)( channel.Value * (flaskHeight-4)/gFlowMeter.TopValue ) + 4;	
	if(oil_level < flaskHeight)
	{
		GUI_SetColor(COLOR_GREY);
		Rect.x0 = x+3;
		Rect.x1 = x+FLASK_WIDTH*width_coeff;
		Rect.y0 = y - flaskHeight+1;
		Rect.y1 = y - oil_level;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//		GUI_FillRectEx(&Rect);
		
		// draw label in flask:
		Rect.y1 = y - flaskHeight + 25;
		GUI_SetColor(GUI_BLACK);
		DrawRectTextInScreenSector(channel.Name, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
//		GUI_DispStringInRectWrap(channel.Name, &Rect, GUI_TA_VCENTER |
//																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		
		// draw oil in flask:
		Rect.y0 = y - oil_level;
		Rect.y1 = y - 4;
		for(uint8_t i=3; i<FLASK_WIDTH+width_coeff; i++)
		{
			Rect.x0 = x+3+(i-3)*width_coeff;
			Rect.x1 = x+2+(i-2)*width_coeff;
			GUI_SetColor(GradientOrange[i-3]);
			FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//			GUI_FillRectEx(&Rect);
		}
		
		// draw output value in flask:
		GUI_SetFont(GUI_FONT_24_1);
		if(channel.Value<1)
			output_value = floor(channel.Value*100+((float)0.5))/100;
		else if(channel.Value<999)
			output_value = floor(channel.Value*10+((float)0.5))/10;
		else
			output_value = 999;
		sprintf (str, "%g", output_value);
		Rect.y0 = y - 30;
		Rect.y1 = y - 3;
		Rect.x0 = x+3;
		Rect.x1 = x+FLASK_WIDTH*width_coeff;
		GUI_SetColor(GUI_BLACK);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
//		GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
//																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	}
	
	// draw input temperature1:
	if(channel.Temperature1<200)
		output_value = floor(channel.Temperature1*10+((float)0.5))/10;
	else
		output_value = 200;
	sprintf (str, "%g�C", output_value);
	Rect.x0 = x+1;
	Rect.x1 = x+FLASK_WIDTH*width_coeff+2;
	Rect.y0 = y - flaskHeight - 28;
	Rect.y1 = y - flaskHeight - 4;
	GUI_SetColor(COLOR_WHITE);
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_DrawRectEx(&Rect);
	DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
//	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
//																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = x;
	Rect.x1 = x+FLASK_WIDTH*width_coeff+3;
	Rect.y0 = y - flaskHeight - 29;
	Rect.y1 = y - flaskHeight - 3;
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_DrawRectEx(&Rect);
	
	if(mode == FLASK_2TH)
	{
		// draw outlet temperature2:
		if(channel.Temperature2<200)
			output_value = floor(channel.Temperature2*10+((float)0.5))/10;
		else
			output_value = 200;
		sprintf (str, "%g�C", output_value);
		Rect.x0 = x+1;
		Rect.x1 = x+FLASK_WIDTH*width_coeff+2;
		Rect.y0 = y + 4;
		Rect.y1 = y + 26;
		GUI_SetColor(COLOR_WHITE);
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
	//	GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
	//	GUI_DrawRectEx(&Rect);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
	//	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
	//																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		Rect.x0 = x;
		Rect.x1 = x+FLASK_WIDTH*width_coeff+3;
		Rect.y0 = y + 3;
		Rect.y1 = y +27;
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//		GUI_DrawRectEx(&Rect);
	}
}

static void DrawLabelsTaho(uint16_t offset, TScreenSector* sector)
{	
	// draw labels =====:		
	GUI_RECT Rect;
	GUI_SetFont(GUI_FONT_24_ASCII);
	GUI_SetColor(COLOR_BK2);
	Rect.y0 = LABEL_Y0;
	Rect.y1 = LABEL_Y1;
	Rect.x0 = LABEL1_X0 + FLOWMETER_PAGE_OFFSET;
	Rect.x1 = LABEL1_X1 + FLOWMETER_PAGE_OFFSET;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	DrawRectTextInScreenSector("INJECTOR", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
//	GUI_DispStringInRectWrap("INJECTOR", &Rect, GUI_TA_VCENTER |
//															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(COLOR_DARK);
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_DrawRectEx(&Rect);	
	
	Rect.y0 = LABEL_DOWN_Y0;
	Rect.y1 = LABEL_DOWN_Y1;
	GUI_SetColor(COLOR_BK2);
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);	
	GUI_SetFont(GUI_FONT_20_1);
	DrawRectTextInScreenSector("All flow values in ml/min", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
//	GUI_DispStringInRectWrap("INJECTOR", &Rect, GUI_TA_VCENTER |
//															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(COLOR_DARK);
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_DrawRectEx(&Rect);
				
	GUI_SetColor(COLOR_BK2);
	
	Rect.y0 = LABEL_Y0;
	Rect.y1 = LABEL_Y1;
	Rect.x0 = LABEL2_X0 + FLOWMETER_PAGE_OFFSET;
	Rect.x1 = LABEL2_X1 + FLOWMETER_PAGE_OFFSET;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	GUI_SetFont(GUI_FONT_24_ASCII);
	DrawRectTextInScreenSector("PUMP", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
//	GUI_DispStringInRectWrap("PUMP", &Rect, GUI_TA_VCENTER |
//															GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	GUI_SetColor(COLOR_DARK);
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_DrawRectEx(&Rect);
	
	// draw taho =====:		
	Rect.y0 = LABEL_DOWN_Y0;
	Rect.y1 = LABEL_DOWN_Y1;
	GUI_SetColor(COLOR_BK2);
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
//	GUI_DrawRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);	
	Rect.x1 = TAHO_X0 + FLOWMETER_PAGE_OFFSET;
	GUI_SetFont(GUI_FONT_20_1);
	DrawRectTextInScreenSector("Speed", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
	Rect.x0 = TAHO_X0 + FLOWMETER_PAGE_OFFSET;
	Rect.x1 = TAHO_X1 + FLOWMETER_PAGE_OFFSET;
	Rect.y0 = TAHO_Y0;
	Rect.y1 = TAHO_Y1;
	GUI_SetColor(COLOR_WHITE);
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
	GUI_SetColor(COLOR_DARK);
	DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
	GUI_SetColor(GUI_BLACK);
	GUI_SetFont(GUI_FONT_24_ASCII);
	char str[5] = "";	
	sprintf(str, "%d", gFlowMeter.Taho);
	DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);	
	Rect.x0 = TAHO_X1 + FLOWMETER_PAGE_OFFSET;
	Rect.x1 = LABEL2_X1 + FLOWMETER_PAGE_OFFSET;	
	GUI_SetFont(GUI_FONT_20_1);
	DrawRectTextInScreenSector("RPM", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);	
}

static void GetTopValue(void)
{		
	float local_max = (float)1.0;
	for(uint8_t i=0; i<CHANNEL_NUM; i++)
	{
		if(local_max < gFlowMeter.Channel[i].Value)
			local_max = gFlowMeter.Channel[i].Value;
	}
	if((float)(gFlowMeter.TopValue*0.9f) <= local_max  ||  (float)(gFlowMeter.TopValue*0.35f) > local_max)
	{
			gFlowMeter.TopValue = local_max*2;
	}
}


// --------------------------------------------------------- Private constants -------------------:

static char ChannelNames[CHANNEL_NUM][15] = {"Delivery",
																			"Back Flow",
																			"Delivery",
																			"Back Flow"}; 

																			

static const uint32_t GradientOrange[FLASK_WIDTH-1] = {
												0x00baf3,
												0x00baf3,
												0x00b7f2,
												0x00b7f2,
												0x00b4f1,
												0x00b1f0,
												0x01adee,
												0x00a9ed,
												0x00a3ec,
												0x009ee9,
												0x0099e7,
												0x0094e6,
												0x018ee3,
												0x0188e2,
												0x0083df,
												0x017ede,
												0x0079dc,
												0x0174db,
												0x0171d9,
												0x006dd8,
												0x0169d7,
												0x0168d6,
												0x0165d5,
												0x0165d5,
												0x0165d5,
												0x0166d5,
												0x0067d6,
												0x006ad7,
												0x0171d9,
												0x0174db,
												0x0079dc,
												0x017ede,
												0x0083df,
												0x0188e2,
												0x018ee3,
												0x0094e6,
												0x0099e7,
												0x009ee9,
												0x00a3ec,
												0x00a9ed,
												0x00a9ed,
												0x01adee,
												0x00b1f0,
												0x00b1f0,
												0x00b4f1,
												0x00b4f1,
												0x00b4f1 };

/**********************************************************************************END OF FILE****/
