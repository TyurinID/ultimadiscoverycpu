/**
  ************************************************************************************************
  * @file	 	 GraphicCommon.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    11-January-2017
  * @brief   Set here colors for GUI.
  *************************************************************************************************
  */

#ifndef GRAPH_COMMON_H
#define GRAPH_COMMON_H
// -------------------------------------------------------- Includes -----------------------------:
#include "cmsis_os.h"
#include "GUI.h"

// -------------------------------------------------------- Exported define ----------------------:
// Device GUI Colors =======:
#define		COLOR_BK1				0x007b5d43
#define		COLOR_BK2				0x00917964		// #define		COLOR_BK2				0x896f59
#define		COLOR_BK2_TR		0x10917964
#define		COLOR_DARK			0x004f2d12
#define		COLOR_WHITE			0x00ece8e1
#define		COLOR_GREY			0x00c5c5c5

#define		COLOR_GRAPH_SCALES	COLOR_WHITE
#define		COLOR_GRAPH_NET			0x44ece8e1
#define		COLOR_GRAPH_CURVE		GUI_RED

// -------------------------------------------------------- Exported types -----------------------:
typedef enum
{
	BACKGROUND ,
	FOREGROUND
}		TLayer;


typedef enum {
	MAIN_WIN,
	SETTINGS_WIN,
	THERMOMETERS_WIN
}	tActiveWindow;

// -------------------------------------------------------- Exported variables -------------------:
extern tActiveWindow 	gActiveWindow; 							// Type of current window to be displaed.

extern GUI_MEMDEV_Handle hMem;

// -------------------------------------------------------- Exported functions -------------------:


#endif
