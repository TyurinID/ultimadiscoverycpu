
#ifndef NETWORK_DLG_H
#define NETWORK_DLG_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include <math.h>
#include "GraphicTask/GraphicTask.h"

#include "GraphicTask/ScreenSectors.h"

#include <string.h>

#include "GraphicTask/GraphicCommon.h"
#include "ApplicationCommon.h"

extern void NetworkDlg_Draw(void);

extern void NetworkDlg_Process(__IO TS_StateTypeDef  ts);

// Fields coordinates =====:
#define		NET1_X0					70
#define		NET1_X1					130
#define		NET2_X0					140
#define		NET2_X1					200
#define		NET3_X0					210
#define		NET3_X1					270
#define		NET4_X0					280
#define		NET4_X1					340

#define		NETLINE1_Y0				45
#define		NETLINE2_Y0				70
#define		NETLINE3_Y0				95
#define		NETLINE4_Y0				120
#define		NETLINE5_Y0				145
#define		NETLINE6_Y0				170
#define		NETLINE7_Y0				195


#endif

