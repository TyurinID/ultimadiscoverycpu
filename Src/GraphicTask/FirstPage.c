/**
  ************************************************************************************************
  * @file	   FirstPage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include	"GraphicTask/FirstPage.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/SettingsWindow.h"
	
// --------------------------------------------------------- Global variables --------------------:
extern GUI_CONST_STORAGE GUI_BITMAP bmlogoBlue;
bool	gStardexLogoDrawn;
uint8_t touchCntFirstPage;
// --------------------------------------------------------- Private variables -------------------:  

// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:
 /***********************
  *		@brief  Initialize all elements of First Page.
  *  	@param  None
  *  	@retval None
  **********************/
void	Init_FirstPage(void)
{
	gStardexLogoDrawn = false;
	touchCntFirstPage = 0;
}	
	
 /***********************
  *		@brief  Draw all elements of First Page.
  *  	@param  None
  *  	@retval None
  **********************/
void	Draw_FirstPage(TScreenSector* sector)
{
		if(gStardexLogoDrawn == false)
			DrawBitmapInScreenSector(&bmlogoBlue, STARDEX_LOGO_X0 + FIRST_PAGE_OFFSET, STARDEX_LOGO_Y0, gCurrentDisplayOffset, sector);
}


 /***********************
  *		@brief  Draw all elements of First Page.
  *  	@param  None
  *  	@retval None
  **********************/
void	Process_FirstPage(__IO TS_StateTypeDef  ts)
{
	// Space under "Stardex" clicked  20 times ================:
	if(ts.touchX[0] > STARDEX_LOGO_X0 && ts.touchX[0] < STARDEX_LOGO_X1 &&
			ts.touchY[0] > STARDEX_LOGO_Y1 + 40 && ts.touchY[0] < 272)
	{
		if(++touchCntFirstPage >= TOUCH_BORDER_VALUE)
		{
			gActiveWindow = SETTINGS_WIN;
			touchCntFirstPage = 0;
			GToDrawFLG = 1;
		}
	}
}
// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
