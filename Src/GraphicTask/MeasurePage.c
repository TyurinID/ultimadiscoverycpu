/**
  ************************************************************************************************
  * @file	   MeasurePage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for Measure Page drawing and processes.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include	"GraphicTask/MeasurePage.h"
#include	"GraphicTask/GraphicCommon.h"
#include  <string.h>
#include	<math.h>



// --------------------------------------------------------- Global variables --------------------:
tMeasured gMeasured;					// Structure for Measured Data.
// --------------------------------------------------------- Private variables -------------------: 
static const char InjTitle[4][8];
// --------------------------------------------------------- Private function prototypes ---------:	
static void drawGauge(int x0, int y0, float value, float top_value, char* units, uint16_t offset, TScreenSector* sector);
//static void calculateTopValue(float* parameter, float* top_value);


// --------------------------------------------------------- Global functions --------------------:
	
	
 /***********************
  *		@brief  Draw all elements of MeasurePage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Draw_MeasurePage(TScreenSector* sector)
{
	GUI_RECT Rect;
	GUI_SetFont(GUI_FONT_24_1);
	
	// Draw "Inj1" ... =======:
	GUI_SetColor(COLOR_WHITE);
	Rect.y0 = BK_LINE_0_Y0;	
	Rect.y1 = BK_LINE_0_Y1;
	for(uint8_t i=0; i<INJECTORS_NUM; i++)
	{	
		Rect.x0 = GAUGES_X0 + i*GAUGES_X_SHIFT + MEASURE_PAGE_OFFSET;
		Rect.x1 = GAUGES_X0 + i*GAUGES_X_SHIFT + MEASURE_PAGE_OFFSET + GAUGE_LINE_WIDTH;
		DrawRectTextInScreenSector(InjTitle[i], Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
	}
	
	// Draw background wide lines ============:
	GUI_SetColor(COLOR_BK2);	
	Rect.x0 = 5 + MEASURE_PAGE_OFFSET;
	Rect.x1 = 475 + MEASURE_PAGE_OFFSET;
	Rect.y0 = BK_LINE_1_Y0;	
	Rect.y1 = BK_LINE_1_Y1;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
	Rect.y0 = BK_LINE_2_Y0;	
	Rect.y1 = BK_LINE_2_Y1;
	FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
	GUI_SetColor(COLOR_WHITE);	
	Rect.y1 = BK_LINE_2_Y0 + BK_TEXT_HEIGHT;	
	DrawRectTextInScreenSector("Resistance", Rect, GUI_TA_BOTTOM | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
			
	if(gMeasured.InjType == INJ_COIL)
	{	
		Rect.y0 = BK_LINE_1_Y0;	
		Rect.y1 = BK_LINE_1_Y0 + BK_TEXT_HEIGHT;
		DrawRectTextInScreenSector("Inductance", Rect, GUI_TA_BOTTOM | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		
		// Draw gauges =============:
		for(uint8_t i=0; i<INJECTORS_NUM; i++)
		{	
			drawGauge(GAUGES_X0 + i*GAUGES_X_SHIFT + MEASURE_PAGE_OFFSET, GAUGES_INDUCTANCE_Y0, gMeasured.Param1[i], GAUGE_COIL_INDUCTANCE_TOP, "uH", gCurrentDisplayOffset, sector);
			drawGauge(GAUGES_X0 + i*GAUGES_X_SHIFT + MEASURE_PAGE_OFFSET, GAUGES_RESISTANCE_Y0, gMeasured.Param2[i], GAUGE_COIL_RESISTANCE_TOP, "mOhm", gCurrentDisplayOffset, sector);
		}
	}
	else if (gMeasured.InjType == INJ_PIEZO)
	{
		Rect.y0 = BK_LINE_1_Y0;	
		Rect.y1 = BK_LINE_1_Y0 + BK_TEXT_HEIGHT;
		DrawRectTextInScreenSector("Capacitance", Rect, GUI_TA_BOTTOM | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
				
		// Draw gauges =============:
		for(uint8_t i=0; i<INJECTORS_NUM; i++)
		{	
			drawGauge(GAUGES_X0 + i*GAUGES_X_SHIFT + MEASURE_PAGE_OFFSET, GAUGES_INDUCTANCE_Y0, gMeasured.Param1[i], GAUGE_PIEZO_CAPACITANCE_TOP, "uF", gCurrentDisplayOffset, sector);
			drawGauge(GAUGES_X0 + i*GAUGES_X_SHIFT + MEASURE_PAGE_OFFSET, GAUGES_RESISTANCE_Y0, gMeasured.Param2[i], GAUGE_PIEZO_RESISTANCE_TOP, "kOhm", gCurrentDisplayOffset, sector);
		}
	}
}

 /***********************
  *		@brief  Initialize all elements of MeasurePage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Init_MeasurePage(void)
{
	gMeasured.Param1[0] = 2.58;
	gMeasured.Param1[1] = 8.8;
	gMeasured.Param1[2] = 5.21;
	gMeasured.Param1[3] = 9.0;
	
	gMeasured.Param2[0] = 250.5;
	gMeasured.Param2[1] = 201.1;
	gMeasured.Param2[2] = 262.5;
	gMeasured.Param2[3] = 150.3;
		
	gMeasured.InjType = INJ_PIEZO;
}


// --------------------------------------------------------- Private functions -------------------:
static void drawGauge(int x0, int y0, float value, float top_value, char* units, uint16_t offset, TScreenSector* sector)
{
		GUI_RECT Rect;
		char str[16] = "";
		GUI_SetFont(GUI_FONT_24_1);
		
		GUI_SetColor(COLOR_BK1);	
		Rect.x0 = x0;
		Rect.x1 = Rect.x0 + GAUGE_LINE_WIDTH;
		Rect.y1 = y0;	
		Rect.y0 = y0 - GAUGE_LINE_HEIGHT;
		FillRectInScreenSector(Rect, offset, sector);	
		GUI_SetColor(COLOR_WHITE);
		DrawRectInScreenSector(Rect, offset, sector);
		
		float relative_value = value/top_value;
		if(relative_value > 1)
			relative_value = 1;
		Rect.x1 = Rect.x0 + relative_value*GAUGE_LINE_WIDTH;		
		FillRectInScreenSector(Rect, offset, sector);		
		Rect.x1 = Rect.x0 + GAUGE_LINE_WIDTH;
		Rect.y0 = y0 - GAUGE_LINE_HEIGHT - GAUGE_NUMBER_HEIGHT;	
		Rect.y1 = y0 - GAUGE_LINE_HEIGHT - 5;
		sprintf(str, "%g\n", value);
		strcat(str, units);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_BOTTOM | GUI_TA_HCENTER, offset, sector);
}

//static void calculateTopValue(float* parameter, float* top_value)
//{
//	float	max_value = 0.0;
//	for(uint8_t i=0; i<INJECTORS_NUM; i++)
//	{
//		if(parameter[i] > max_value)
//			max_value = parameter[i];
//	}
//	if((*top_value*MAX_PERCENT_BORDER) <= max_value  ||  (*top_value*MIN_PERCENT_BORDER) > max_value)
//		*top_value = max_value*1.5;	
//}

// --------------------------------------------------------- Private constants -------------------:
static const char InjTitle[4][8] = {
														"Inj I",
														"Inj II",
														"Inj III",
														"Inj IV" };


/**********************************************************************************END OF FILE****/
