/**
  ************************************************************************************************
  * @file	 	 GraphicTask.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    11-January-2017
  * @brief   
  *************************************************************************************************
  */

#ifndef GRAPHICTASK_H
#define GRAPHICTASK_H


#define GRAPHIC_THREAD_DELAY			500		//ms
#define GRAPHIC_PERIOD_MS					10
// Error timer =============:
#define ERROR_POINT_TIME_MS			2000		

// -------------------------------------------------------- Includes -----------------------------:
#include "cmsis_os.h"
#include "GraphicTask/GraphicCommon.h"

// -------------------------------------------------------- Exported functions -------------------:
extern void GraphicThread(void const *argument);    	// graphic task for main application


#endif

