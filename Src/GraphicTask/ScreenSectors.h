/**
  ************************************************************************************************
  * @file		 ScreenSectors.h
  * @author  Tyurin Ivan
  * @version V2.0
  * @date    13-January-2016
  * @brief   Header for ScreenSectors.c.
  *************************************************************************************************
  */

#ifndef SCREEN_SECTORS_H
#define SCREEN_SECTORS_H

// -------------------------------------------------------- Includes -----------------------------:
#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include "cmsis_os.h"

// -------------------------------------------------------- Exported define ----------------------:
#define	PAGE_X_SIZE				480
#define	VIRTUAL_DISPLAY_X_SIZE	960	// in pxls
#define SCREEN_SECTORS_NUM 4

// -------------------------------------------------------- Exported types -----------------------: 
typedef struct
{
	int FXBegin;
	int FXEnd;
	int FYBegin;
	int FYEnd;
} TScreenSector;


// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern TScreenSector GScreen_sector[SCREEN_SECTORS_NUM];
extern uint16_t gCurrentDisplayOffset;							// Coordinate in virtual display space
																										// from which drawing starts.

// -------------------------------------------------------- Exported functions -------------------:
extern void InitializeScreenSectors(void);

extern void DrawBitmapInScreenSector(const GUI_BITMAP * pBM, int x0, int y0, uint16_t offset, TScreenSector* screen_sector); 
extern void DrawLineInScreenSector(int x1, int y1, int x2, int y2, uint16_t offset, TScreenSector* screen_sector);
extern void DrawRectInScreenSector(GUI_RECT pRect, uint16_t offset, TScreenSector* screen_sector);
extern void FillRectInScreenSector(GUI_RECT pRect, uint16_t offset, TScreenSector* screen_sector);
extern void DrawTextInScreenSector(const char *s, int x1, int y1, uint16_t offset, TScreenSector* screen_sector);
extern void DrawRectTextInScreenSector(const char *s, GUI_RECT pRect, int TextAlign,  uint16_t offset, TScreenSector* screen_sector);

#endif

