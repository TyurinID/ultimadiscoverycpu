
#include "GraphicTask/NetworkDlg.h"

#include "GraphicTask/GraphKeyboard.h"			

void NetworkDlg_Process(__IO TS_StateTypeDef  ts)
{
	if( NETLINE2_Y0 < ts.touchY[0] && ts.touchY[0] < NETLINE3_Y0 )		// Ip address:
	{
		if( NET1_X0 < ts.touchX[0] && ts.touchX[0] < NET1_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GIpAddress.byte1) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET2_X0 < ts.touchX[0] && ts.touchX[0] < NET2_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GIpAddress.byte2) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET3_X0 < ts.touchX[0] && ts.touchX[0] < NET3_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GIpAddress.byte3) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET4_X0 < ts.touchX[0] && ts.touchX[0] < NET4_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GIpAddress.byte4) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
	}
	else	if( NETLINE4_Y0 < ts.touchY[0] && ts.touchY[0] < NETLINE5_Y0 )		// Net Mask:
	{
		if( NET1_X0 < ts.touchX[0] && ts.touchX[0] < NET1_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GNetMask.byte1) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET2_X0 < ts.touchX[0] && ts.touchX[0] < NET2_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GNetMask.byte2) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET3_X0 < ts.touchX[0] && ts.touchX[0] < NET3_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GNetMask.byte3) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET4_X0 < ts.touchX[0] && ts.touchX[0] < NET4_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GNetMask.byte4) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
	}
	else	if( NETLINE6_Y0 < ts.touchY[0] && ts.touchY[0] < NETLINE7_Y0 )		// Default Gateway:
	{
		if( NET1_X0 < ts.touchX[0] && ts.touchX[0] < NET1_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GDefaultGateway.byte1) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET2_X0 < ts.touchX[0] && ts.touchX[0] < NET2_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GDefaultGateway.byte2) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET3_X0 < ts.touchX[0] && ts.touchX[0] < NET3_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GDefaultGateway.byte3) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
		if( NET4_X0 < ts.touchX[0] && ts.touchX[0] < NET4_X1 )
		{
			GKeyBoard.FMin = 0;
			GKeyBoard.FMax = 256;
			GKeyBoard.Target = (uint32_t*)(&(GDefaultGateway.byte4) );
			GKeyBoard.Mode = KB_DRAW_FRAME;
			GKeyBoard.Active = 1;
			GToDrawFLG = 1;	
		}
	}	
}

void NetworkDlg_Draw(void)
{	
	GUI_SetFont(&GUI_Font20_1);
	GUI_SetTextMode(GUI_TM_TRANS);
	GUI_RECT Rect;
	char str[5];

	// draw big frame:
	Rect.x0 = NET1_X0-10;
	Rect.x1 = NET4_X1+10;
	Rect.y0 = NETLINE1_Y0-5;
	Rect.y1 = NETLINE7_Y0+5;
	GUI_SetColor(COLOR_BK2);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	Rect.x0 = NET1_X0-9;
	Rect.x1 = NET4_X1+9;
	Rect.y0 = NETLINE1_Y0-4;
	Rect.y1 = NETLINE7_Y0+4;
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	
	// draw labels:
	Rect.x0 = NET1_X0;
	Rect.x1 = NET4_X1;
	Rect.y0 = NETLINE1_Y0;
	Rect.y1 = NETLINE2_Y0;
	GUI_SetColor(COLOR_DARK);
	GUI_DispStringInRectWrap("IP Address:", &Rect, GUI_TA_VCENTER |
																				GUI_TA_LEFT, GUI_WRAPMODE_WORD);
	Rect.y0 = NETLINE3_Y0;
	Rect.y1 = NETLINE4_Y0;
	GUI_DispStringInRectWrap("Net Mask:", &Rect, GUI_TA_VCENTER |
																				GUI_TA_LEFT, GUI_WRAPMODE_WORD);	
	Rect.y0 = NETLINE5_Y0;
	Rect.y1 = NETLINE6_Y0;
	GUI_DispStringInRectWrap("Default Gateway:", &Rect, GUI_TA_VCENTER |
																				GUI_TA_LEFT, GUI_WRAPMODE_WORD);
			
	// draw fields and text in netline1 "IP Address":
	Rect.y0 = NETLINE2_Y0;
	Rect.y1 = NETLINE3_Y0;
	Rect.x0 = NET1_X0;
	Rect.x1 = NET1_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GIpAddress.byte1);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET2_X0;
	Rect.x1 = NET2_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GIpAddress.byte2);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET3_X0;
	Rect.x1 = NET3_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GIpAddress.byte3);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET4_X0;
	Rect.x1 = NET4_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GIpAddress.byte4);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
																	
	// draw fields and text in netline2 "Net Mask":
	Rect.y0 = NETLINE4_Y0;
	Rect.y1 = NETLINE5_Y0;
	Rect.x0 = NET1_X0;
	Rect.x1 = NET1_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GNetMask.byte1);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET2_X0;
	Rect.x1 = NET2_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GNetMask.byte2);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET3_X0;
	Rect.x1 = NET3_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GNetMask.byte3);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET4_X0;
	Rect.x1 = NET4_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GNetMask.byte4);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);															
	
		// draw fields and text in netline3 "Default Gateway":
	Rect.y0 = NETLINE6_Y0;
	Rect.y1 = NETLINE7_Y0;
	Rect.x0 = NET1_X0;
	Rect.x1 = NET1_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GDefaultGateway.byte1);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET2_X0;
	Rect.x1 = NET2_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GDefaultGateway.byte2);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET3_X0;
	Rect.x1 = NET3_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GDefaultGateway.byte3);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x0 = NET4_X0;
	Rect.x1 = NET4_X1;
	GUI_SetColor(COLOR_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(COLOR_DARK);
	GUI_DrawRectEx(&Rect);
	sprintf(str, "%d", (uint8_t)GDefaultGateway.byte4);
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																	GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
}


