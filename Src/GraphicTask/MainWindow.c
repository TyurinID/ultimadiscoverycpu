/**
  ************************************************************************************************
  * @file	   MainWindow.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    11-January-2017
  * @brief   File with functions for Main Window drawing and process.
  *************************************************************************************************
  */
// -------------------------------------------------------- Includes -----------------------------:	
#include "GraphicTask/MainWindow.h"
#include "GraphicTask/SettingsWindow.h"
#include "GraphicTask/ScreenSectors.h"
#include <math.h>	

#include	"GraphicTask/FirstPage.h"
#include	"GraphicTask/AmpGraphPage.h"
#include	"GraphicTask/FlowMeterPage.h"
#include	"GraphicTask/DelayGraphPage.h"
#include	"GraphicTask/ProfilePage.h"
#include	"GraphicTask/MeasurePage.h"


// --------------------------------------------------------- Global variables --------------------:
const uint32_t GradientOrange[FLASK_WIDTH-1];				// For orange gradient.

// --------------------------------------------------------- Private variables -------------------:
static tActivePage		ActivePage; 
static uint16_t				xInitial;
static int						xDiff;
static tShift					ScreenShift;
static uint16_t				NewDisplayOffset;

// --------------------------------------------------------- Private function prototypes ---------:						
			

// --------------------------------------------------------- Global functions --------------------:

void MainWin_Init(void)
{
	ActivePage = FIRST_PAGE;
	
	GUI_SetTextMode(GUI_TM_TRANS);
	
	Init_FirstPage();
	Init_AmpGraphPage();
	Init_FlowMeterPage();
	Init_DelayGraphPage();
	Init_ProfilePage();
	Init_MeasurePage();
}


 /***********************
  *		@brief  Draw pages on main window. Detect touch shifting.
  *  	@param  touched_prev, ts.
  *  	@retval None
  **********************/
void MainWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts)
{	
	// Shift action =================================================:
	if(ScreenShift == TO_LEFT_PAGE)
	{
		if(gCurrentDisplayOffset > NewDisplayOffset)			
				gCurrentDisplayOffset-=X_SHIFT_STEP;
		else
		{
			ActivePage--;
			gCurrentDisplayOffset = NewDisplayOffset;
			touchCntFirstPage = 0;
			ScreenShift = NO_SHIFT;
		}
	}
	else if(ScreenShift == TO_RIGHT_PAGE)
	{
		if(gCurrentDisplayOffset < NewDisplayOffset)			
				gCurrentDisplayOffset+=X_SHIFT_STEP;
		else
		{
			ActivePage++;
			gCurrentDisplayOffset = NewDisplayOffset;
			touchCntFirstPage = 0;
			ScreenShift = NO_SHIFT;
		}
	}
	else if(ScreenShift == NO_SHIFT)
	{
		// Execute touch screen ======================================:
		uint8_t touch = ts.touchDetected;
		if(touch != 0)		
		{
			if(touched_prev == 0)			// Just touched ==:
			{
				xDiff = 0;
				xInitial = ts.touchX[0];
				Process_FirstPage(ts);
			}
			else			// Touched for long time:
				xDiff = ts.touchX[0] - xInitial;
		}
		else	if(touched_prev != 0)		// Just untouched ==:
		{
			// To shift or not ==================================:
			if(xDiff >= X_DIFF_BOUNDARY && ActivePage != FIRST_PAGE)
			{		
					NewDisplayOffset = gCurrentDisplayOffset - PAGE_X_SIZE;
					ActivePage--;
					ScreenShift = TO_LEFT_PAGE;
			}
			else if(xDiff <= (-X_DIFF_BOUNDARY) && ActivePage != MEASURE_PAGE)
			{		
					NewDisplayOffset = gCurrentDisplayOffset + PAGE_X_SIZE;
					ActivePage++;
					ScreenShift = TO_RIGHT_PAGE;
			}
			else
					ScreenShift = NO_SHIFT;
		}
	}
	
	// Draw active page or shift process ===============:
	for(uint8_t i=0; i<SCREEN_SECTORS_NUM; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYBegin, 
														 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin - 4, 
														 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();
	
		switch (ActivePage)
		{
			default:
			case FIRST_PAGE:
				Draw_FirstPage(&GScreen_sector[i]);
				break;
			
			case FIRST_AND_AMPGRAPH_PAGES:
				Draw_FirstPage(&GScreen_sector[i]);
				Draw_AmpGraphPage(&GScreen_sector[i]);
				break;
			
			case AMPGRAPH_PAGE:
				Draw_AmpGraphPage(&GScreen_sector[i]);
				break;
			
			case AMPGRAPH_AND_FLOWMETER_PAGES:
				Draw_AmpGraphPage(&GScreen_sector[i]);
				Draw_FlowMeterPage(&GScreen_sector[i]);
				break;
			
			case FLOWMETER_PAGE:
				Draw_FlowMeterPage(&GScreen_sector[i]);
				break;
			
			case FLOWMETER_AND_DELAYGRAPH_PAGES:
				Draw_FlowMeterPage(&GScreen_sector[i]);
				Draw_DelayGraphPage(&GScreen_sector[i]);
				break;
			
			case DELAYGRAPH_PAGE:
				Draw_DelayGraphPage(&GScreen_sector[i]);
				break;			
			
			case DELAYGRAPH_AND_PROFILE_PAGES:
				Draw_DelayGraphPage(&GScreen_sector[i]);
				Draw_ProfilePage(&GScreen_sector[i]);
				break;
			
			case PROFILE_PAGE:
				Draw_ProfilePage(&GScreen_sector[i]);
				break;			
			
			case PROFILE_AND_MEASURE_PAGES:
				Draw_ProfilePage(&GScreen_sector[i]);
				Draw_MeasurePage(&GScreen_sector[i]);
				break;
			
			case MEASURE_PAGE:
				Draw_MeasurePage(&GScreen_sector[i]);
				break;
		}

	
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}


// --------------------------------------------------------- Private functions -------------------:



// --------------------------------------------------------- Private constants -------------------:




/**********************************************************************************END OF FILE****/
