/**
  *************************************************************************************************
  * @file	 	 Graph.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    12-January-2017
  * @brief   Here is Graph Control functions.
  *************************************************************************************************
  */

// -------------------------------------------------------- Includes -----------------------------:  
#include "GraphicTask/Graph.h"
#include <string.h>
#include <math.h>

// --------------------------------------------------------- Global variables --------------------:
  
// --------------------------------------------------------- Private variables -------------------:  
 static const float axisStep[GRAPH_STEPS_NUM];
// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:

  
 /***********************
  *		@brief  Initialization of previously configured tGraph structure.
	*						Should be done before drawing.
  *  	@param  graph - previously configured tGraph structure.
  *  	@retval Returns 0 in case of success;
	*						returns -1 in case of error.
  **********************/
int	InitializeGraph(tGraph* graph)
{
	int ret = 0;
	uint16_t x_resolution = graph->x1 - graph->x0 - GRAPH_X_AUTO_INDENT;
	uint16_t y_resolution = graph->y1 - graph->y0 - GRAPH_Y_AUTO_INDENT - GRAPH_AXIS_WIDTH;
	
	// Calculate maximum and minimum Values ===:
	float x_max_value = graph->xValue[0];
	float x_min_value = graph->xValue[0];
	float y_max_value = graph->yValue[0];
	float y_min_value = graph->yValue[0];
	for(uint16_t i=0; i<graph->PointsNum; i++)
	{	
		if(graph->xValue[i] > x_max_value)
			x_max_value = graph->xValue[i];
		else if(graph->xValue[i] < x_min_value)
			x_min_value = graph->xValue[i];
		
		if(graph->yValue[i] > y_max_value)
			y_max_value = graph->yValue[i];
		else if(graph->yValue[i] < y_min_value)
			y_min_value = graph->yValue[i];	
	}
	
	
	// Calculate net lines steps ==============:
	float	x_net_lines_step = axisStep[0];
	float	y_net_lines_step = axisStep[0];
	float x_step_diffrence_min = 5.0;
	float y_step_diffrence_min = 5.0;
	float step_diffrence;
	for(uint8_t i=0; i<GRAPH_STEPS_NUM; i++)
	{
		step_diffrence = (x_max_value - x_min_value)/axisStep[i] - GRAPH_X_NETLINES_NUM;
		if( step_diffrence >= 0 && step_diffrence < x_step_diffrence_min)
		{
			x_step_diffrence_min = step_diffrence;
			x_net_lines_step = axisStep[i];
		}
		step_diffrence = (y_max_value - y_min_value)/axisStep[i] - GRAPH_Y_NETLINES_NUM;
		if( step_diffrence >= 0 && step_diffrence < y_step_diffrence_min)
		{
			y_step_diffrence_min = step_diffrence;
			y_net_lines_step = axisStep[i];
		}		
	}	
		
	// Negative correction ==================:
	float	 	x_negative_corr = 0.0;
	float	 	y_negative_corr = 0.0;
	if(x_min_value < 0)
		x_negative_corr = (((-x_min_value)/x_net_lines_step) - (uint16_t)((-x_min_value)/x_net_lines_step))*x_net_lines_step;
	if(y_min_value < 0)
		y_negative_corr = (((-y_min_value)/y_net_lines_step) - (uint16_t)((-y_min_value)/y_net_lines_step))*y_net_lines_step;
	
	// min_value = lowest NetLine ========:	
	if(y_negative_corr != 0.0f)		
	{
		y_min_value = roundf((y_min_value + y_negative_corr - y_net_lines_step)*10)/10;
		y_step_diffrence_min += 1.0f;
	}
	if(x_negative_corr != 0.0f)	
	{
		x_step_diffrence_min += 1.0f;
		x_min_value = roundf((x_min_value + x_negative_corr - x_net_lines_step)*10)/10;
	}		
	
	// Check, are y Values in range ===========:
	if((y_max_value - y_min_value) < axisStep[GRAPH_STEPS_NUM-1]*(GRAPH_Y_NETLINES_NUM-1))
	{		
		graph->yNetLinesNum =  (uint8_t)(GRAPH_Y_NETLINES_NUM + 1);
		y_net_lines_step = axisStep[GRAPH_STEPS_NUM-5];
	}	
	
	// Write two parameters to structure ===:
	graph->xNetLinesNum =  (uint8_t)( ceilf(x_step_diffrence_min) + GRAPH_X_NETLINES_NUM);
	graph->yNetLinesNum =  (uint8_t)( ceilf(y_step_diffrence_min) + GRAPH_Y_NETLINES_NUM);
	
	// Write two parameters to structure ===:
	graph->xMinValue = x_min_value;
	graph->yMinValue = y_min_value;	
	
	// Fill netLines coordinates ========:
	for(uint8_t i=0; i<=graph->xNetLinesNum; i++)
	{
		graph->xNetLineValue[i] = roundf((x_min_value +  i*x_net_lines_step)*10)/10;
	}
	for(uint8_t i=0; i<=graph->yNetLinesNum; i++)
	{
		graph->yNetLineValue[i] = roundf((y_min_value +  i*y_net_lines_step)*10)/10;
	}
	
	// Calculate converting coefficients and start coordinates ======:
	if(graph->Indent.xLeft == 1)
	{			
		if(graph->Indent.xRight == 1)
			graph->xCoeff = (float)x_resolution/((graph->xNetLinesNum*x_net_lines_step) + 2*x_net_lines_step);
		else
			graph->xCoeff = (float)x_resolution/((graph->xNetLinesNum*x_net_lines_step) + x_net_lines_step);
		// Write parameter to structure ===:
		graph->xStartCoord = graph->x0 + x_net_lines_step*graph->xCoeff;
	}	
	else	
	{		
		if(graph->Indent.xRight == 1)
				graph->xCoeff = (float)x_resolution/((graph->xNetLinesNum*x_net_lines_step) + x_net_lines_step);	
		else
				graph->xCoeff = (float)x_resolution/(graph->xNetLinesNum*x_net_lines_step);	
	  // Write parameter to structure ===:
		graph->xStartCoord = graph->x0;
	}
	
	if(graph->Indent.yBottom == 1)
	{	
		if(graph->Indent.yTop == 1)
			graph->yCoeff = (float)y_resolution/((graph->yNetLinesNum*y_net_lines_step) + 2*y_net_lines_step);
		else
			graph->yCoeff = (float)y_resolution/((graph->yNetLinesNum*y_net_lines_step) + y_net_lines_step);
		// Write parameter to structure ===:
		graph->yStartCoord = graph->y1 - y_net_lines_step*graph->yCoeff;
	}	
	else	
	{	
		if(graph->Indent.yTop == 1)
			graph->yCoeff = (float)y_resolution/((graph->yNetLinesNum*y_net_lines_step) + 1*y_net_lines_step);
		else
			graph->yCoeff = (float)y_resolution/(graph->yNetLinesNum*y_net_lines_step) ;
		// Write parameter to structure ===:
		graph->yStartCoord = graph->y1 - GRAPH_AXIS_WIDTH;
	}
	
	return ret;
}




 /***********************
  *		@brief  Drawing of previously configured and Initialized tGraph structure.
  *  	@param  graph - previously configured tGraph structure.
  *  	@retval None
  **********************/
void	DrawGraph(tGraph graph, uint16_t offset, TScreenSector* sector)
{
	// Draw net lines =============:
	GUI_RECT Rect;
	GUI_SetPenSize(1);
	int x0,x1,y0,y1;
	char str[10] = "";
	// X netlines:
	GUI_SetFont(GUI_FONT_20_1);
	for(uint16_t i=0; i<=graph.xNetLinesNum; i++)
	{
		if(graph.NetON)
		{
			GUI_SetColor(graph.Color.Net);
			x0 = graph.xStartCoord + (graph.xNetLineValue[i] - graph.xMinValue)*graph.xCoeff;
			x1 = x0;
			y0 = graph.y0;
			y1 = graph.y1;
			DrawLineInScreenSector(x0, y0, x1, y1, offset, sector);
//			GUI_DrawLine(x0, y0, x1, y1);
		}
		GUI_SetColor(graph.Color.Axis);
		Rect.x0 = x0;
		Rect.x1 = x0 + 1;
		Rect.y0 = graph.y1 - GRAPH_AXIS_WIDTH - 2;
		Rect.y1 = graph.y1;
		FillRectInScreenSector(Rect, offset, sector);
//		GUI_FillRect(x0, y0, x1, y1);
		Rect.x0 = x0 - 20;
		Rect.x1 = x0 + 20;
		Rect.y0 = y1 + GRAPH_NUMBERS_INDENT;
		Rect.y1 = y1 + GRAPH_NUMBERS_INDENT + 20;
		if(i%2 == 0 || graph.xNetLineValue[i] == 0)			// only each second number should be displayed
		{
			GUI_SetColor(graph.Color.Names);
			sprintf(str, "%g", graph.xNetLineValue[i]);
			DrawRectTextInScreenSector(str, Rect, GUI_TA_TOP | GUI_TA_HCENTER, offset, sector);
//			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_TOP |
//																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		}
	}
	// Y netlines:
	for(uint16_t i=0; i<=graph.yNetLinesNum; i++)
	{
		if(graph.NetON)
		{
			GUI_SetColor(graph.Color.Net);
			x0 = graph.x0 ;
			x1 = graph.x1;
			y1 = graph.yStartCoord - (graph.yNetLineValue[i] - graph.yMinValue)*graph.yCoeff;
			y0 = y1;
			DrawLineInScreenSector(x0, y0, x1, y1, offset, sector);
//			GUI_DrawLine(x0, y0, x1, y1);
		}
		GUI_SetColor(graph.Color.Axis);
		Rect.x0 = x0;
		Rect.x1 = x0 + GRAPH_AXIS_WIDTH + 2;
		Rect.y0 = y1 - 1;
		Rect.y1 = y1;
		FillRectInScreenSector(Rect, offset, sector);
//		GUI_FillRect(x0, y0, x1, y1);
		Rect.x0 = x0 - GRAPH_NUMBERS_INDENT - 40;
		Rect.x1 = x0 - GRAPH_NUMBERS_INDENT;
		Rect.y0 = y1 - 12;
		Rect.y1 = y1 + 12;
		if(i%2 == 0 || graph.yNetLineValue[i] == 0)				// only each second number should be displayed
		{
			GUI_SetColor(graph.Color.Names);
			sprintf(str, "%g", graph.yNetLineValue[i]);
			DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_RIGHT, offset, sector);
//			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
//																GUI_TA_RIGHT, GUI_WRAPMODE_WORD);
		}
	}	

	// Curve ==========:
	GUI_SetColor(graph.Color.Curve);
	GUI_SetPenSize(2);
	for(uint16_t i=0; i<graph.PointsNum-1; i++)
	{
		x0 = graph.xStartCoord + (graph.xValue[i] - graph.xMinValue)*graph.xCoeff ;
		x1 = graph.xStartCoord + (graph.xValue[i+1] - graph.xMinValue)*graph.xCoeff ;
		y0 = graph.yStartCoord - (graph.yValue[i] - graph.yMinValue)*graph.yCoeff ;
		y1 = graph.yStartCoord - (graph.yValue[i+1] - graph.yMinValue)*graph.yCoeff ;
		DrawLineInScreenSector(x0, y0, x1, y1, offset, sector);
//		GUI_DrawLine(x0, y0, x1, y1);
	}
	
	// Marks ===========:
	if(graph.xMarkON)
	{
		GUI_SetColor(graph.Color.Axis);
		uint16_t	marker_line_step	= (graph.y1 - graph.y0 - 10)/6;
		x0 = graph.xStartCoord + (graph.xMarkValue - graph.xMinValue)*graph.xCoeff;
		x1 = x0;
		for(uint8_t i=0; i<6; i++)
		{			
			y1 = graph.y1 - i*marker_line_step;
			y0 = y1 - marker_line_step/2;
			DrawLineInScreenSector(x0, y0, x1, y1, offset, sector);			
		}
		// Value:
		GUI_SetColor(graph.Color.Bk);
		if(graph.xMarkAlignment == MARK_RIGHT)
		{
			Rect.x0 = x0 + 2;
			Rect.x1 = x0 + strlen(graph.xMarkSignature)*11 + 2;
		}
		else
		{
			Rect.x1 = x0 - 2;
			Rect.x0 = Rect.x1 - strlen(graph.xMarkSignature)*11;
		}
		Rect.y0 = y0 - 30;
		Rect.y1 = y0 - 5;
		FillRectInScreenSector(Rect, offset, sector);		
		GUI_SetColor(graph.Color.Axis);
		DrawRectInScreenSector(Rect, offset, sector);	
		DrawRectTextInScreenSector(graph.xMarkSignature, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, offset, sector);
	}
		
	GUI_SetColor(graph.Color.Axis);
	// Abscissa =======:
	Rect.x0 = graph.x0;
	Rect.x1 = graph.x1;
	Rect.y0 = graph.y1 - GRAPH_AXIS_WIDTH + 1;
	Rect.y1 = graph.y1;
	FillRectInScreenSector(Rect, offset, sector);
//	GUI_FillRectEx(&Rect);
	// Ordinate =======:
	Rect.x1 = graph.x0 + GRAPH_AXIS_WIDTH - 1;
	Rect.y0 = graph.y0;
	Rect.y1 = graph.y1;
	FillRectInScreenSector(Rect, offset, sector);
//	GUI_FillRectEx(&Rect);
	
	// Axises names =============:
	GUI_SetTextMode(GUI_TM_NORMAL);
	Rect.x0 = graph.x0 + GRAPH_Y_NAME_X_INDENT;
	Rect.x1 = Rect.x0 + strlen(graph.yName)*11 + 5;
	Rect.y0 = graph.y0 - GRAPH_Y_NAME_Y_INDENT;
	Rect.y1 = Rect.y0 + 20;
	DrawRectTextInScreenSector(graph.yName, Rect, GUI_TA_TOP | GUI_TA_LEFT, offset, sector);
//	GUI_DispStringInRectWrap(graph.yName, &Rect, GUI_TA_VCENTER |
//																GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
	Rect.x1 = graph.x1 - GRAPH_X_NAME_X_INDENT;
	Rect.x0 = Rect.x1 - strlen(graph.xName)*11 - 2;
	Rect.y1 = graph.y1 - GRAPH_X_NAME_Y_INDENT;
	Rect.y0 = Rect.y1 - 20;
	DrawRectTextInScreenSector(graph.xName, Rect, GUI_TA_VCENTER | GUI_TA_RIGHT, offset, sector);
//	GUI_DispStringInRectWrap(graph.xName, &Rect, GUI_TA_VCENTER |
//																GUI_TA_RIGHT, GUI_WRAPMODE_WORD);
	GUI_SetTextMode(GUI_TM_TRANS);	
}	


// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:
static const float axisStep[GRAPH_STEPS_NUM] = {
																	5000,
																	2000,
																	1000,
																	750,
																	600,
																	500,
																	400,
																	250,
																	200,
																	150,
																	125,
																	100,
																	90,
																	80,
																	70,
																	60,
																	50,
																	40,
																	30,
																	25,
																	20,
																	15,
																	10,
																	8,
																	6,
																	5,
																	4.0f,
																	3.0f,
																	2.0f,
																	1.0f,
																	0.8f,
																	0.75f,
																	0.6f,
																	0.5f,
																	0.4f,
																	0.3f,
																	0.25f,
																	0.2f,
																	0.1f,
																	0.05f};


/**********************************************************************************END OF FILE****/
