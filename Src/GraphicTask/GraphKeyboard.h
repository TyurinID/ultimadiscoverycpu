
#ifndef GRAPH_KEYBOARD_H
#define GRAPH_KEYBOARD_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/SettingsWindow.h"

#include <math.h>
#include <string.h>

#define KEY_BOARD_DIGITS 11
typedef enum
{
  KB_NORMAL  = 0, 
  KB_DRAW_FRAME,
	KB_DRAW_ADDIT
} TKbMode;

typedef struct
{
	uint32_t 			*Target;
	float		 			*AdditTarget;			// Only for SensorDLG;
	uint8_t 			digits[KEY_BOARD_DIGITS];
	uint8_t 			digits_length;
	
	uint16_t			Coord[4];		// x0, x1, y0, y1 .
	uint8_t 			Active;  // active
	TKbMode				Mode;		// Only for SensorDLG;
	uint32_t 			FMin;		// min value
	uint32_t 			FMax;  	// max value
	
} TKeyBoard;

;

extern TKeyBoard GKeyBoard;

extern void KeyBoard_Init(void);			// TI: keyboard characteristics on the screen

extern void KeyBoard_Draw(void);			// TI: 

extern void KeyBoard_Process(__IO TS_StateTypeDef  ts);		// TI: 

// Keboard coordinates:
#define COLUMN1_X0	195
#define COLUMN1_X1	283
#define COLUMN2_X0	286
#define COLUMN2_X1	373
#define COLUMN3_X0	376
#define COLUMN3_X1	465

#define LINE1_Y0		20
#define LINE1_Y1		62
#define LINE2_Y0		65
#define LINE2_Y1		107
#define LINE3_Y0		110
#define LINE3_Y1		152
#define LINE4_Y0		155
#define LINE4_Y1		200

#endif
