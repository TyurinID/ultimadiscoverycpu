/**
  ************************************************************************************************
  * @file	 	 MainWindow.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    11-January-2017
  * @brief   Header for MainWindow.c.Set here coordinates of all field.
  *************************************************************************************************
  */
#ifndef MAIN_WINDOW
#define MAIN_WINDOW
// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"
#include "GUI.h"

#include "stm32746g_discovery_ts.h"
#include <string.h>
#include <math.h>

#include "GraphicTask/GraphicTask.h"


// -------------------------------------------------------- Exported define ----------------------:
#define   X_DIFF_BOUNDARY		30				// in pxls
#define 	X_SHIFT_STEP			40				// 480 must be divisible evenly (10; 30; 40; 120)

// For Graph structure ==============================:
#define 	TIME_POINT_SHIFT	3.8f			// ms  0.95 * 4
#define		AMP_TO_PXLS				11.17f		// pxls = amp/AMP_TO_PXLS


// Buttons coordinates ====:
#define		BUTTON_Y0			228
#define		BUTTON_Y1			267

#define		BUTTON1_X0			3
#define		BUTTON1_X1			117

#define		BUTTON2_X0			122
#define		BUTTON2_X1			236

#define		BUTTON3_X0			351
#define		BUTTON3_X1			475

//// Flasks =================:
//#define 	FLASKS_SHIFT		54
//#define		FLASK_HEIGHT		146
//#define		FLASK_WIDTH			48

//// Labels coordinates =====:
//#define 	LABEL_Y0				2
//#define 	LABEL_Y1				24
//#define 	LABEL1_X0				47
//#define 	LABEL1_X1				259

//#define 	LABEL2_X0				263
//#define 	LABEL2_X1				475

//// Taho coordinates =======:
//#define 	TAHO_BIG_X0			240
//#define 	TAHO_BIG_X1			346

//#define 	TAHO_X0					245
//#define 	TAHO_X1					297
//#define 	TAHO_Y0					236
//#define 	TAHO_Y1					259



// -------------------------------------------------------- Exported types -----------------------:
// Type of active page on Main Window ====================:
typedef enum
{
	FIRST_PAGE,
	FIRST_AND_AMPGRAPH_PAGES,
	AMPGRAPH_PAGE,
	AMPGRAPH_AND_FLOWMETER_PAGES,
	FLOWMETER_PAGE,
	FLOWMETER_AND_DELAYGRAPH_PAGES,
	DELAYGRAPH_PAGE,
	DELAYGRAPH_AND_PROFILE_PAGES,
	PROFILE_PAGE,
	PROFILE_AND_MEASURE_PAGES,
	MEASURE_PAGE
}	tActivePage;

// Screen shift type:
typedef enum
{
	NO_SHIFT,
	TO_RIGHT_PAGE,
	TO_LEFT_PAGE
}	tShift;

// List of window sections to be redrawn =================:
typedef enum
{
  DO_NOT_DRAW  = 0,
  DRAW_LABELS_AND_TAHO,				// Labels "Delivery" & "Back Flow", taho field and "RPM".
  DRAW_PUMP,
	DRAW_UNITS,
	DRAW_START,
	DRAW_ALL
} TDrawButtonsAndLabels;



// -------------------------------------------------------- Exported variables -------------------:
extern TDrawButtonsAndLabels 		GDrawButtonsAndLabels;				// FLG To draw buttons and labels.	

// bmp pictures for display:
extern GUI_CONST_STORAGE GUI_BITMAP bmButtonYellow;
extern GUI_CONST_STORAGE GUI_BITMAP bmButtonRed;
extern GUI_CONST_STORAGE GUI_BITMAP bmButtonGreen;

// -------------------------------------------------------- Exported functions -------------------:

extern	void MainWin_Process(uint8_t touched_prev, __IO TS_StateTypeDef  ts);			
extern 	void MainWin_Init(void);

// -------------------------------------------------------- Exported constants -------------------:


#endif
/**********************************************************************************END OF FILE****/
