/**
  ************************************************************************************************
  * @file	   AmpGraphPage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
#ifndef AMP_GRAPH_PAGE_H
#define AMP_GRAPH_PAGE_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"
#include "GraphicTask/Graph.h"


// -------------------------------------------------------- Exported define ----------------------:
// Graph coordinates ======:
#define 	GRAPH_X0			40
#define		GRAPH_X1			460
#define 	GRAPH_Y0			30
#define		GRAPH_Y1			240

// Current page offset =======:
#define		AMPGRAPH_PAGE_OFFSET	480		// x coordinate in virtual display space.
	
	
// -------------------------------------------------------- Exported types -----------------------: 
 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern tGraph gAmpGraph;					// Structure for Amp Graphic.



// -------------------------------------------------------- Exported functions -------------------: 
extern void	Draw_AmpGraphPage(TScreenSector* sector);
extern void	Init_AmpGraphPage(void);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
