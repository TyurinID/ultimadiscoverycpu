/**
  ************************************************************************************************
  * @file	   ProfilePage.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   .
  *************************************************************************************************
  */
#ifndef PROFILE_PAGE_H
#define PROFILE_PAGE_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"
#include	"GraphicTask/ScreenSectors.h"


// -------------------------------------------------------- Exported define ----------------------:
// Current page offset =======:
#define		PROFILE_PAGE_OFFSET		1920		// x coordinate in virtual display space.


// Page graphic coordinates ======:
#define	LABEL_A_X0		15	
#define	LABEL_A_X1		230
#define	LABEL_B_X0		250	
#define	LABEL_B_X1		465

#define	VALUE_A_X0		145
#define	VALUE_A_X1		220
#define	VALUE_B_X0		380
#define	VALUE_B_X1		455

#define	LABEL_1_Y0		20	
#define	LABEL_1_Y1		70	
#define	LABEL_2_Y0		80	
#define	LABEL_2_Y1		130	
#define	LABEL_3_Y0		140	
#define	LABEL_3_Y1		190	
#define	LABEL_4_Y0		200	
#define	LABEL_4_Y1		250	

#define LABEL_INDENT		7

// -------------------------------------------------------- Exported types -----------------------: 
 typedef struct
 {
	float			BoostI;
	float			BoostU;
	float			FirstI;
	float			FirstW;
	float			SecondI;
	float			BatteryU;
	float			NegativeU1;
	float			NegativeU2;
 }	tProfile;
 
 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern tProfile  gProfile;
 
 
// -------------------------------------------------------- Exported functions -------------------: 
extern void	Draw_ProfilePage(TScreenSector* sector);
extern void	Init_ProfilePage(void);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
