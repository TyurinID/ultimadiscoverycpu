/**
  ************************************************************************************************
  * @file	   ProfilePage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
	
// -------------------------------------------------------- Includes -----------------------------:  
#include	"GraphicTask/ProfilePage.h"
#include	"GraphicTask/GraphicCommon.h"
	
// --------------------------------------------------------- Global variables --------------------:
tProfile  gProfile;
// --------------------------------------------------------- Private variables -------------------:  
  
// --------------------------------------------------------- Private function prototypes ---------:	
  
// --------------------------------------------------------- Global functions --------------------:

  //GUI_Clear();
//	GUI_RECT Rect;
//	GUI_SetTextMode(GUI_TM_TRANS);
//	GUI_SetColor(COLOR_DARK);
//	GUI_SetFont(GUI_FONT_24_ASCII);
	
	
 /***********************
  *		@brief  Draw all elements of ProfilePage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Draw_ProfilePage(TScreenSector* sector)
{	
		GUI_RECT Rect;
		char str[16] = "";
		GUI_SetFont(GUI_FONT_24_1);
	
		// Boost I =============:
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_1_Y0;
		Rect.y1 = LABEL_1_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("Boost I", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_1_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_1_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g A", gProfile.BoostI);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		
		// Boost U =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_1_Y0;
		Rect.y1 = LABEL_1_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("Boost U", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_1_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_1_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g V", gProfile.BoostU);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);	
		
		// First I =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_2_Y0;
		Rect.y1 = LABEL_2_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("First I", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_2_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_2_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g A", gProfile.FirstI);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
				
		// First W =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_2_Y0;
		Rect.y1 = LABEL_2_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("First W", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_2_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_2_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g us", gProfile.FirstW);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
				
		// Second I =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_3_Y0;
		Rect.y1 = LABEL_3_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("Second I", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_3_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_3_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g A", gProfile.SecondI);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		
		// Battery U =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_3_Y0;
		Rect.y1 = LABEL_3_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("Battery U", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_3_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_3_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g V", gProfile.BatteryU);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
				
		// Negative U1 =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_4_Y0;
		Rect.y1 = LABEL_4_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("Negative U1", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_A_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_A_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_4_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_4_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g V", gProfile.NegativeU1);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
				
		// Negative U2 =============:	
		GUI_SetColor(COLOR_BK2);
		Rect.x0 = LABEL_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = LABEL_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_4_Y0;
		Rect.y1 = LABEL_4_Y1;	
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);		
		Rect.x1 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		DrawRectTextInScreenSector("Negative U2", Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		GUI_SetColor(COLOR_WHITE);
		Rect.x0 = VALUE_B_X0 + PROFILE_PAGE_OFFSET;
		Rect.x1 = VALUE_B_X1 + PROFILE_PAGE_OFFSET;
		Rect.y0 = LABEL_4_Y0 + LABEL_INDENT;
		Rect.y1 = LABEL_4_Y1 - LABEL_INDENT;
		FillRectInScreenSector(Rect, gCurrentDisplayOffset, sector);	
		GUI_SetColor(COLOR_DARK);
		DrawRectInScreenSector(Rect, gCurrentDisplayOffset, sector);
		sprintf(str, "%g V", gProfile.NegativeU2);
		DrawRectTextInScreenSector(str, Rect, GUI_TA_VCENTER | GUI_TA_HCENTER, gCurrentDisplayOffset, sector);
		
		
}

 /***********************
  *		@brief  Initialize all elements of ProfilePage.
  *  	@param  None
  *  	@retval None
  **********************/
void	Init_ProfilePage(void)
{
	gProfile.BoostU = 60;
	gProfile.BoostI = 21.5;
	gProfile.FirstI = 20;
	gProfile.FirstW = 450;
	gProfile.SecondI = 12;
	gProfile.BatteryU = 12;
	gProfile.NegativeU1 = 60;
	gProfile.NegativeU2 = 48;
}


// --------------------------------------------------------- Private functions -------------------:

// --------------------------------------------------------- Private constants -------------------:

/**********************************************************************************END OF FILE****/
