/**
  ************************************************************************************************
  * @file	   DelayGraphPage.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
#ifndef DELAY_GRAPH_PAGE_H
#define DELAY_GRAPH_PAGE_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"
#include "GraphicTask/Graph.h"


// -------------------------------------------------------- Exported define ----------------------:
// Current page offset =======:
#define		DELAYGRAPH_PAGE_OFFSET	1440		// x coordinate in virtual display space.
  
// Graph coordinates ======:
#define 	GRAPH_X0			40
#define		GRAPH_X1			460
#define 	GRAPH_Y0			30
#define		GRAPH_Y1			240

#define		DELAY_VALUE_X0	160
#define		DELAY_VALUE_X1	260
#define		DELAY_VALUE_Y0	20
#define		DELAY_VALUE_Y1	80

// -------------------------------------------------------- Exported types -----------------------: 
 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern tGraph gDelayGraph;					// Structure for Delay Graphic.
 
 
// -------------------------------------------------------- Exported functions -------------------: 
extern void	Draw_DelayGraphPage(TScreenSector* sector);
extern void	Init_DelayGraphPage(void);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
