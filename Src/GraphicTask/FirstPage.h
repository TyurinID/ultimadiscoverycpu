/**
  ************************************************************************************************
  * @file	   FirstPage.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    13-January-2017
  * @brief   File with functions for First Page drawing and processes.
  *************************************************************************************************
  */
#ifndef FIRST_PAGE_H
#define FIRST_PAGE_H	
// -------------------------------------------------------- Includes -----------------------------: 
#include "main.h"
#include "GUI.h"

#include "GraphicTask/ScreenSectors.h"

// -------------------------------------------------------- Exported define ----------------------:
// Current page offset =======:
#define		FIRST_PAGE_OFFSET		0		// x coordinate in virtual display space.

#define		STARDEX_LOGO_X0			90  
#define		STARDEX_LOGO_Y0			100  
#define 	STARDEX_LOGO_X1			390  
#define		STARDEX_LOGO_Y1			174 

#define		TOUCH_BORDER_VALUE	10
	
	
// -------------------------------------------------------- Exported types -----------------------: 
 
// -------------------------------------------------------- Private macros -----------------------:
 
// -------------------------------------------------------- Exported variables -------------------: 
extern uint8_t touchCntFirstPage;
// -------------------------------------------------------- Exported functions -------------------: 
extern void	Draw_FirstPage(TScreenSector* sector);
extern void	Init_FirstPage(void);
extern void	Process_FirstPage(__IO TS_StateTypeDef  ts);
 
// -------------------------------------------------------- Exported constants -------------------: 
 
 
 #endif
/**********************************************************************************END OF FILE****/ 
