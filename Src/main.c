/**
  ************************************************************************************************
  * @file	 	 main.c
  * @author  Tyurin Ivan
  * @version V1.2
  * @date    15-January-2017
  * @brief   Main function file for DiscoveryCPU firmware.
	*					 Peripheral initialization functions.
	*					 General threads of FreeRTOS start here.
  *************************************************************************************************
  */


// -------------------------------------------------------- Includes -----------------------------:  
#include "main.h"
#include "cmsis_os.h"

#ifdef 	ETHERNET_TCP_IP
//// Ethernet TCP/IP ========:
#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "ethernetif.h"
#include "app_ethernet.h"
#endif

// Threads functions are contained in the following header files:
#include "Application/Application.h"
#include "GraphicTask/GraphicTask.h"
#include "Uart/uart.h"


	
#include "GraphicTask/GraphicCommon.h"



// --------------------------------------------------------- Private function prototypes ---------:	
static void SystemClock_Config(void);				
static void MPU_Config(void);
static void CPU_CACHE_Enable(void);

// --------------------------------------------------------- Global functions --------------------: 



/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @main
  *	&		@brief  Main function. 
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
int main(void)
{
  // Configure the MPU attributes as Write Through =:
  MPU_Config();

  // Enable the CPU Cache :
  CPU_CACHE_Enable();

  // STM32F7xx HAL library initialization :
  HAL_Init();  
  
  // Configure the system clock to 216 MHz :
  SystemClock_Config(); 
	// Update core clock value for FreeRTOS :
	SystemCoreClock = HAL_RCC_GetHCLKFreq();  
		
	// Init the STemWin GUI Library ========:
  BSP_SDRAM_Init(); 		// Initializes the SDRAM device.
  __HAL_RCC_CRC_CLK_ENABLE(); // Enable the CRC Module.
  GUI_Init();
  
	// Set string on display ===============: 
  GUI_DispStringAt("Starting...", 0, 0);


//	// Configure the RTC peripheral ====:
//	RTC_Init();
  
//	// Tags initialization by zero:
//	MakeTagsPadding();
	
  
  // Initialaze threads =================================================================:
	
	osThreadDef(Application, ApplicationThread, osPriorityNormal, 0, 8 * configMINIMAL_STACK_SIZE);	// ApplicationThread do all calculations with data from oval sensors.
	osThreadDef(Graphic, GraphicThread, osPriorityNormal, 0, 8 * configMINIMAL_STACK_SIZE);			// GraphicThread supports display and touchscreen working.
	osThreadDef(UART, UART_Thread, osPriorityNormal, 0, 8 * configMINIMAL_STACK_SIZE);				// Conversation with MainCPU.

	osThreadCreate (osThread(Application), NULL);	
	osThreadCreate (osThread(Graphic), NULL);
	osThreadCreate(osThread(UART), NULL);

	// Start scheduler =======:
  osKernelStart();
  
  // We should never get here as control is now taken by the scheduler.
  for( ;; );
}



/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @vApplicationStackOverflowHook
  *	&	@brief  Executing in case of stack overflow. 
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void vApplicationStackOverflowHook(void)
{
	while(1)
	{
		HAL_Delay(10);
	}
}	





// -------------------------------------------------------- Private functions --------------------:
/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 216000000
  *            HCLK(Hz)                       = 216000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 432;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;

  ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
  if(ret != HAL_OK)
  {
    while(1) { ; }
  }

  /* Activate the OverDrive to reach the 216 MHz Frequency */
  ret = HAL_PWREx_EnableOverDrive();
  if(ret != HAL_OK)
  {
    while(1) { ; }
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
  if(ret != HAL_OK)
  {
    while(1) { ; }
  }
}

/**
  * @brief  Configure the MPU attributes as Write Through for SRAM1/2.
  * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
  *         The Region Size is 256KB, it is related to SRAM1 and SRAM2  memory size.
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;
  
  /* Disable the MPU */
  HAL_MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  
  /* Infinite loop */
  while (1)
  {
  }
}
#endif

// --------------------------------------------------------- Private constants -------------------:



/**********************************************************************************END OF FILE****/
